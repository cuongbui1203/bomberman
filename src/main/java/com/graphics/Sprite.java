package com.graphics;

import com.hs.ConstGame;
import javafx.scene.image.*;

import java.util.Arrays;

/**
 * class kiểm soát các pixel.
 */
public class Sprite {
    private static final Sprite[] skins = {
            // 96

            // up 1 2 3
            // left 1 2 3
            // down 1 2 3
            // right 1 2 3
            // 1
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 1, 3, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 0, 3, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 2, 3, SpriteSheet.tiles2, 32, 32),

            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 1, 1, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 0, 1, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 2, 1, SpriteSheet.tiles2, 32, 32),

            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 1, 0, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 0, 0, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 2, 0, SpriteSheet.tiles2, 32, 32),

            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 1, 2, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 0, 2, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 2, 2, SpriteSheet.tiles2, 32, 32),

            // 2
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 4, 3, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 3, 3, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 5, 3, SpriteSheet.tiles2, 32, 32),

            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 4, 1, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 3, 1, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 5, 1, SpriteSheet.tiles2, 32, 32),

            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 4, 0, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 3, 0, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 5, 0, SpriteSheet.tiles2, 32, 32),

            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 4, 2, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 3, 2, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 5, 2, SpriteSheet.tiles2, 32, 32),

            // 3
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 7, 3, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 6, 3, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 8, 3, SpriteSheet.tiles2, 32, 32),

            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 7, 1, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 6, 1, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 8, 1, SpriteSheet.tiles2, 32, 32),

            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 7, 0, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 6, 0, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 8, 0, SpriteSheet.tiles2, 32, 32),

            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 7, 2, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 6, 2, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 8, 2, SpriteSheet.tiles2, 32, 32),

            // 4
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 10, 3, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 9, 3, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 11, 3, SpriteSheet.tiles2, 32, 32),

            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 10, 1, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 9, 1, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 11, 1, SpriteSheet.tiles2, 32, 32),

            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 10, 0, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 9, 0, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 11, 0, SpriteSheet.tiles2, 32, 32),

            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 10, 2, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 9, 2, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 11, 2, SpriteSheet.tiles2, 32, 32),

            // 5
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 1, 7, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 0, 7, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 2, 7, SpriteSheet.tiles2, 32, 32),

            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 1, 5, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 0, 5, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 2, 5, SpriteSheet.tiles2, 32, 32),

            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 1, 4, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 0, 4, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 2, 4, SpriteSheet.tiles2, 32, 32),

            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 1, 6, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 0, 6, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 2, 6, SpriteSheet.tiles2, 32, 32),

            // 6
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 4, 7, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 3, 7, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 5, 7, SpriteSheet.tiles2, 32, 32),

            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 4, 5, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 3, 5, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 5, 5, SpriteSheet.tiles2, 32, 32),

            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 4, 4, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 3, 4, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 5, 4, SpriteSheet.tiles2, 32, 32),

            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 4, 6, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 3, 6, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 5, 6, SpriteSheet.tiles2, 32, 32),

            // 7
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 7, 7, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 6, 7, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 8, 7, SpriteSheet.tiles2, 32, 32),

            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 7, 5, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 6, 5, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 8, 5, SpriteSheet.tiles2, 32, 32),

            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 7, 4, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 6, 4, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 8, 4, SpriteSheet.tiles2, 32, 32),

            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 7, 6, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 6, 6, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 8, 6, SpriteSheet.tiles2, 32, 32),

            // 8
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 10, 7, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 9, 7, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 11, 7, SpriteSheet.tiles2, 32, 32),

            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 10, 5, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 9, 5, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 11, 5, SpriteSheet.tiles2, 32, 32),

            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 10, 4, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 9, 4, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 11, 4, SpriteSheet.tiles2, 32, 32),

            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 10, 6, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 9, 6, SpriteSheet.tiles2, 32, 32),
            new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 11, 6, SpriteSheet.tiles2, 32, 32),
    };
    /*
     * |--------------------------------------------------------------------------
     * | Board sprites
     * |--------------------------------------------------------------------------
     */
    public static Sprite grass = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 6, 0, SpriteSheet.tiles, 16, 16);
    public static Sprite brick = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 7, 0, SpriteSheet.tiles, 16, 16);
    public static Sprite brick_plus = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 8, 0, SpriteSheet.tiles, 16, 16);
    public static Sprite brick_plus_1 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 8, 1, SpriteSheet.tiles, 16, 16);
    public static Sprite wall = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 5, 0, SpriteSheet.tiles, 16, 16);
    public static Sprite portal = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 4, 0, SpriteSheet.tiles, 14, 14);
    /*
     * |--------------------------------------------------------------------------
     * | Character
     * |--------------------------------------------------------------------------
     */
    // BALLOM
    public static Sprite balloom_left1 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 9, 0, SpriteSheet.tiles, 16, 16);
    public static Sprite balloom_left2 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 9, 1, SpriteSheet.tiles, 16, 16);
    public static Sprite balloom_left3 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 9, 2, SpriteSheet.tiles, 16, 16);
    public static Sprite balloom_right1 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 10, 0, SpriteSheet.tiles, 16,
            16);
    public static Sprite balloom_right2 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 10, 1, SpriteSheet.tiles, 16,
            16);
    public static Sprite balloom_right3 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 10, 2, SpriteSheet.tiles, 16,
            16);
    public static Sprite balloom_dead = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 9, 3, SpriteSheet.tiles, 16, 16);
    // ONEAL
    public static Sprite oneal_left1 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 11, 0, SpriteSheet.tiles, 16, 16);
    public static Sprite oneal_left2 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 11, 1, SpriteSheet.tiles, 16, 16);
    public static Sprite oneal_left3 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 11, 2, SpriteSheet.tiles, 16, 16);
    public static Sprite oneal_right1 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 12, 0, SpriteSheet.tiles, 16, 16);
    public static Sprite oneal_right2 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 12, 1, SpriteSheet.tiles, 16, 16);
    public static Sprite oneal_right3 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 12, 2, SpriteSheet.tiles, 16, 16);
    public static Sprite oneal_dead = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 11, 3, SpriteSheet.tiles, 16, 16);
    // Doll
    public static Sprite doll_left1 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 13, 0, SpriteSheet.tiles, 16, 16);
    public static Sprite doll_left2 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 13, 1, SpriteSheet.tiles, 16, 16);
    public static Sprite doll_left3 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 13, 2, SpriteSheet.tiles, 16, 16);
    public static Sprite doll_right1 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 14, 0, SpriteSheet.tiles, 16, 16);
    public static Sprite doll_right2 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 14, 1, SpriteSheet.tiles, 16, 16);
    public static Sprite doll_right3 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 14, 2, SpriteSheet.tiles, 16, 16);
    public static Sprite doll_dead = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 13, 3, SpriteSheet.tiles, 16, 16);
    // Minvo
    public static Sprite minvo_left1 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 8, 5, SpriteSheet.tiles, 16, 16);
    public static Sprite minvo_left2 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 8, 6, SpriteSheet.tiles, 16, 16);
    public static Sprite minvo_left3 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 8, 7, SpriteSheet.tiles, 16, 16);
    public static Sprite minvo_right1 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 9, 5, SpriteSheet.tiles, 16, 16);
    public static Sprite minvo_right2 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 9, 6, SpriteSheet.tiles, 16, 16);
    public static Sprite minvo_right3 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 9, 7, SpriteSheet.tiles, 16, 16);
    public static Sprite minvo_dead = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 8, 8, SpriteSheet.tiles, 16, 16);
    // Kondoria
    public static Sprite kondoria_left1 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 10, 5, SpriteSheet.tiles, 16,
            16);
    public static Sprite kondoria_left2 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 10, 6, SpriteSheet.tiles, 16,
            16);
    public static Sprite kondoria_left3 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 10, 7, SpriteSheet.tiles, 16,
            16);
    public static Sprite kondoria_right1 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 11, 5, SpriteSheet.tiles, 16,
            16);
    public static Sprite kondoria_right2 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 11, 6, SpriteSheet.tiles, 16,
            16);
    public static Sprite kondoria_right3 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 11, 7, SpriteSheet.tiles, 16,
            16);
    public static Sprite kondoria_dead = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 10, 8, SpriteSheet.tiles, 16,
            16);
    // Ming
    public static Sprite ming_left1 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 4, 5, SpriteSheet.tiles, 16, 16);
    public static Sprite ming_left2 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 4, 6, SpriteSheet.tiles, 16, 16);
    public static Sprite ming_left3 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 4, 7, SpriteSheet.tiles, 16, 16);
    public static Sprite ming_right1 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 5, 5, SpriteSheet.tiles, 16, 16);
    public static Sprite ming_right2 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 5, 6, SpriteSheet.tiles, 16, 16);
    public static Sprite ming_right3 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 5, 7, SpriteSheet.tiles, 16, 16);
    public static Sprite ming_dead = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 4, 8, SpriteSheet.tiles, 16, 16);
    // Cuonh
    public static Sprite Cuonh_left1 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 6, 5, SpriteSheet.tiles, 16, 16);
    public static Sprite Cuonh_left2 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 6, 6, SpriteSheet.tiles, 16, 16);
    public static Sprite Cuonh_left3 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 6, 7, SpriteSheet.tiles, 16, 16);
    public static Sprite Cuonh_right1 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 7, 5, SpriteSheet.tiles, 16, 16);
    public static Sprite Cuonh_right2 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 7, 6, SpriteSheet.tiles, 16, 16);
    public static Sprite Cuonh_right3 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 7, 7, SpriteSheet.tiles, 16, 16);
    public static Sprite Cuonh_dead = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 6, 8, SpriteSheet.tiles, 16, 16);
    // Mun
    public static Sprite Mun_left1 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 12, 5, SpriteSheet.tiles, 16, 16);
    public static Sprite Mun_left2 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 12, 6, SpriteSheet.tiles, 16, 16);
    public static Sprite Mun_left3 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 12, 7, SpriteSheet.tiles, 16, 16);
    public static Sprite Mun_right1 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 13, 5, SpriteSheet.tiles, 16, 16);
    public static Sprite Mun_right2 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 13, 6, SpriteSheet.tiles, 16, 16);
    public static Sprite Mun_right3 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 13, 7, SpriteSheet.tiles, 16, 16);
    public static Sprite Mun_dead = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 12, 8, SpriteSheet.tiles, 16, 16);
    // Cuon
    public static Sprite Cuon_left1 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 14, 5, SpriteSheet.tiles, 16, 16);
    public static Sprite Cuon_left2 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 14, 6, SpriteSheet.tiles, 16, 16);
    public static Sprite Cuon_left3 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 14, 7, SpriteSheet.tiles, 16, 16);
    public static Sprite Cuon_right1 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 15, 5, SpriteSheet.tiles, 16, 16);
    public static Sprite Cuon_right2 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 15, 6, SpriteSheet.tiles, 16, 16);
    public static Sprite Cuon_right3 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 15, 7, SpriteSheet.tiles, 16, 16);
    public static Sprite Cuon_dead = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 14, 8, SpriteSheet.tiles, 16, 16);
    // Smlie and Angry
    public static Sprite Angry_left1 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 8, 13, SpriteSheet.tiles, 16, 16);
    public static Sprite Angry_left2 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 8, 14, SpriteSheet.tiles, 16, 16);
    public static Sprite Angry_left3 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 8, 15, SpriteSheet.tiles, 16, 16);
    public static Sprite Angry_right1 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 9, 13, SpriteSheet.tiles, 16, 16);
    public static Sprite Angry_right2 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 9, 14, SpriteSheet.tiles, 16, 16);
    public static Sprite Angry_right3 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 9, 15, SpriteSheet.tiles, 16, 16);
    public static Sprite Smlie_left1 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 8, 10, SpriteSheet.tiles, 16, 16);
    public static Sprite Smlie_left2 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 8, 11, SpriteSheet.tiles, 16, 16);
    public static Sprite Smlie_left3 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 8, 12, SpriteSheet.tiles, 16, 16);
    public static Sprite Smlie_right1 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 9, 10, SpriteSheet.tiles, 16, 16);
    public static Sprite Smlie_right2 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 9, 11, SpriteSheet.tiles, 16, 16);
    public static Sprite Smlie_right3 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 9, 12, SpriteSheet.tiles, 16, 16);
    public static Sprite Angry_dead = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 10, 13, SpriteSheet.tiles, 16, 16);
    // ALL
    public static Sprite mob_dead1 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 15, 0, SpriteSheet.tiles, 16, 16);
    public static Sprite mob_dead2 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 15, 1, SpriteSheet.tiles, 16, 16);
    public static Sprite mob_dead3 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 15, 2, SpriteSheet.tiles, 16, 16);
    /*
     * |--------------------------------------------------------------------------
     * | Bomb Sprites
     * |--------------------------------------------------------------------------
     */
    public static Sprite bomb = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 0, 3, SpriteSheet.tiles, 15, 15);
    public static Sprite bomb_1 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 1, 3, SpriteSheet.tiles, 13, 15);
    public static Sprite bomb_2 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 2, 3, SpriteSheet.tiles, 12, 14);
    /*
     * |--------------------------------------------------------------------------
     * | FlameSegment Sprites
     * |--------------------------------------------------------------------------
     */
    public static Sprite bomb_exploded = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 0, 4, SpriteSheet.tiles, 16, 16);
    public static Sprite bomb_exploded1 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 0, 5, SpriteSheet.tiles, 16,
            16);
    public static Sprite bomb_exploded2 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 0, 6, SpriteSheet.tiles, 16,
            16);
    public static Sprite explosion_vertical = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 1, 5, SpriteSheet.tiles, 16,
            16);
    public static Sprite explosion_vertical1 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 2, 5, SpriteSheet.tiles,
            16, 16);
    public static Sprite explosion_vertical2 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 3, 5, SpriteSheet.tiles,
            16, 16);
    public static Sprite explosion_horizontal = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 1, 7, SpriteSheet.tiles,
            16, 16);
    public static Sprite explosion_horizontal1 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 1, 8, SpriteSheet.tiles,
            16, 16);
    public static Sprite explosion_horizontal2 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 1, 9, SpriteSheet.tiles,
            16, 16);
    public static Sprite explosion_horizontal_left_last = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 0, 7,
            SpriteSheet.tiles, 16, 16);
    public static Sprite explosion_horizontal_left_last1 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 0, 8,
            SpriteSheet.tiles, 16, 16);
    public static Sprite explosion_horizontal_left_last2 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 0, 9,
            SpriteSheet.tiles, 16, 16);
    public static Sprite explosion_horizontal_right_last = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 2, 7,
            SpriteSheet.tiles, 16, 16);
    public static Sprite explosion_horizontal_right_last1 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 2, 8,
            SpriteSheet.tiles, 16, 16);
    public static Sprite explosion_horizontal_right_last2 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 2, 9,
            SpriteSheet.tiles, 16, 16);
    public static Sprite explosion_vertical_top_last = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 1, 4,
            SpriteSheet.tiles, 16, 16);
    public static Sprite explosion_vertical_top_last1 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 2, 4,
            SpriteSheet.tiles, 16, 16);
    public static Sprite explosion_vertical_top_last2 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 3, 4,
            SpriteSheet.tiles, 16, 16);
    public static Sprite explosion_vertical_down_last = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 1, 6,
            SpriteSheet.tiles, 16, 16);
    public static Sprite explosion_vertical_down_last1 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 2, 6,
            SpriteSheet.tiles, 16, 16);
    public static Sprite explosion_vertical_down_last2 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 3, 6,
            SpriteSheet.tiles, 16, 16);
    /*
     * |--------------------------------------------------------------------------
     * | Brick FlameSegment
     * |--------------------------------------------------------------------------
     */
    public static Sprite brick_exploded = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 7, 1, SpriteSheet.tiles, 16,
            16);
    public static Sprite brick_exploded1 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 7, 2, SpriteSheet.tiles, 16,
            16);
    public static Sprite brick_exploded2 = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 7, 3, SpriteSheet.tiles, 16,
            16);
    /*
     * |--------------------------------------------------------------------------
     * | Powerups
     * |--------------------------------------------------------------------------
     */
    public static Sprite powerup_bombs = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 0, 10, SpriteSheet.tiles, 16,
            16);
    public static Sprite powerup_flames = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 1, 10, SpriteSheet.tiles, 16,
            16);
    public static Sprite powerup_speed = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 2, 10, SpriteSheet.tiles, 16,
            16);
    public static Sprite powerup_wallpass = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 3, 10, SpriteSheet.tiles, 16,
            16);
    public static Sprite powerup_detonator = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 4, 10, SpriteSheet.tiles, 16,
            16);
    public static Sprite powerup_bombpass = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 5, 10, SpriteSheet.tiles, 16,
            16);
    public static Sprite powerup_flamepass = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 6, 10, SpriteSheet.tiles, 16,
            16);
    /*
     * |--------------------------------------------------------------------------
     * | Teleport
     * |--------------------------------------------------------------------------
     */
    public static Sprite teleport = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 6, 11, SpriteSheet.tiles, 16, 16);
    /*
     * |--------------------------------------------------------------------------
     * | Teleport
     * |--------------------------------------------------------------------------
     */
    /*
    * tem
    * */
    public static Sprite bac = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE,0,0,SpriteSheet.tilesbac,16,16);
    public static Sprite vang = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE,0,0,SpriteSheet.tilesvang,16,16);
    public static Sprite kc = new Sprite(ConstGame.InfoObject.DEFAULT_SIZE,0,0,SpriteSheet.tileskc,16,16);
    public static Image icon = new Image(Sprite.class.getResource(ConstGame.InfoGame.ICON_PATH).toString());
    public static Image icon_1 = new Image(Sprite.class.getResource(ConstGame.InfoGame.ICON_2_PATH).toString());

    public final int SIZE;
    public final int[] _pixels;
    protected int _realWidth;
    protected int _realHeight;
    private int _x, _y;
    private SpriteSheet _sheet;

    public Sprite(int size, int x, int y, SpriteSheet sheet, int rw, int rh) {
        SIZE = size;
        _pixels = new int[SIZE * SIZE];
        _x = x * SIZE;
        _y = y * SIZE;
        _sheet = sheet;
        _realWidth = rw;
        _realHeight = rh;
        load();
    }

    public Sprite(int size, int x, int y, int colorPlayer, SpriteSheet sheet, int rw, int rh) {
        SIZE = size;
        _pixels = new int[SIZE * SIZE];
        _x = x * SIZE;
        _y = y * SIZE;
        _sheet = sheet;
        _realWidth = rw;
        _realHeight = rh;
        load(colorPlayer);
    }

    public Sprite(int size, int color) {
        SIZE = size;
        _pixels = new int[SIZE * SIZE];
        setColor(color);
    }

    // ------------------- skin --------------------------------
    public static Sprite player_up_skin(int num) {
        return skins[numSpriteInArr(num, 0, 1)];
    }

    public static Sprite player_down_skin(int num) {
        return skins[numSpriteInArr(num, 2, 1)];
        // return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 2, 0, num,
        // SpriteSheet.tiles, 12, 15);
        // return new Sprite(32,0,0,SpriteSheet.tiles2,32,32);
        // return new
        // Sprite(ConstGame.InfoObject.DEFAULT_SIZE,1,0,SpriteSheet.tiles2,32,32);
    }

    public static Sprite player_left_skin(int num) {
        return skins[numSpriteInArr(num, 1, 1)];
        // return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 3, 0, num,
        // SpriteSheet.tiles, 10, 15);
    }

    public static Sprite player_right_skin(int num) {
        // return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 1, 0, num,
        // SpriteSheet.tiles, 10, 16);
        return skins[numSpriteInArr(num, 3, 1)];
    }

    public static Sprite player_up_1_skin(int num) {
//        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 0, 1, num, SpriteSheet.tiles, 12, 16);
        return skins[numSpriteInArr(num,0,2)];
    }

    public static Sprite player_up_2_skin(int num) {
//        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 0, 2, num, SpriteSheet.tiles, 12, 15);
        return skins[numSpriteInArr(num,0,3)];
    }

    public static Sprite player_down_1_skin(int num) {
//        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 2, 1, num, SpriteSheet.tiles, 12, 15);
         return skins[numSpriteInArr(num,2,2)];
        // Sprite(ConstGame.InfoObject.DEFAULT_SIZE,0,0,SpriteSheet.tiles2,64,64);

    }

    public static Sprite player_down_2_skin(int num) {
//        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 2, 2, num, SpriteSheet.tiles, 12, 16);
         return skins[numSpriteInArr(num,2,3)];
        // Sprite(ConstGame.InfoObject.DEFAULT_SIZE,2,0,SpriteSheet.tiles2,32,32);

    }

    public static Sprite player_left_1_skin(int num) {
//        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 3, 1, num, SpriteSheet.tiles, 11, 16);
        return skins[numSpriteInArr(num,1,2)];
    }

    public static Sprite player_left_2_skin(int num) {
//        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 3, 2, num, SpriteSheet.tiles, 12, 16);
        return skins[numSpriteInArr(num,1,3)];
    }

    public static Sprite player_right_1_skin(int num) {
//        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 1, 1, num, SpriteSheet.tiles, 11, 16);
        return skins[numSpriteInArr(num,3,2)];
    }

    public static Sprite player_right_2_skin(int num) {
//        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 1, 2, num, SpriteSheet.tiles, 12, 16);
        return skins[numSpriteInArr(num,3,3)];
    }

    /*
     * |--------------------------------------------------------------------------
     * | Bomber Sprites
     * |--------------------------------------------------------------------------
     */
    public static Sprite player_nu_up(int color) {
        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 0, 12, color, SpriteSheet.tiles, 12, 16);
    }

    public static Sprite player_nu_down(int color) {
        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 2, 12, color, SpriteSheet.tiles, 12, 15);
    }

    public static Sprite player_nu_left(int color) {
        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 3, 12, color, SpriteSheet.tiles, 10, 15);
    }

    public static Sprite player_nu_right(int color) {
        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 1, 12, color, SpriteSheet.tiles, 10, 16);
    }

    public static Sprite player_nu_up_1(int color) {
        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 0, 13, color, SpriteSheet.tiles, 12, 16);
    }

    public static Sprite player_nu_up_2(int color) {
        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 0, 14, color, SpriteSheet.tiles, 12, 15);
    }

    public static Sprite player_nu_down_1(int color) {
        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 2, 13, color, SpriteSheet.tiles, 12, 15);
    }

    public static Sprite player_nu_down_2(int color) {
        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 2, 14, color, SpriteSheet.tiles, 12, 16);
    }

    public static Sprite player_nu_left_1(int color) {
        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 3, 13, color, SpriteSheet.tiles, 11, 16);
    }

    public static Sprite player_nu_left_2(int color) {
        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 3, 14, color, SpriteSheet.tiles, 12, 16);
    }

    public static Sprite player_nu_right_1(int color) {
        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 1, 13, color, SpriteSheet.tiles, 11, 16);
    }

    public static Sprite player_nu_right_2(int color) {
        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 1, 14, color, SpriteSheet.tiles, 12, 16);
    }

    // ---------------------------------------------------
    public static Sprite player_up(int color) {
        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 0, 0, color, SpriteSheet.tiles, 12, 16);
    }

    public static Sprite player_down(int color) {
        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 2, 0, color, SpriteSheet.tiles, 12, 15);
    }

    public static Sprite player_left(int color) {
        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 3, 0, color, SpriteSheet.tiles, 10, 15);
    }

    public static Sprite player_right(int color) {
        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 1, 0, color, SpriteSheet.tiles, 10, 16);
    }

    public static Sprite player_up_1(int color) {
        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 0, 1, color, SpriteSheet.tiles, 12, 16);
    }

    public static Sprite player_up_2(int color) {
        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 0, 2, color, SpriteSheet.tiles, 12, 15);
    }

    public static Sprite player_down_1(int color) {
        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 2, 1, color, SpriteSheet.tiles, 12, 15);
    }

    public static Sprite player_down_2(int color) {
        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 2, 2, color, SpriteSheet.tiles, 12, 16);
    }

    public static Sprite player_left_1(int color) {
        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 3, 1, color, SpriteSheet.tiles, 11, 16);
    }

    public static Sprite player_left_2(int color) {
        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 3, 2, color, SpriteSheet.tiles, 12, 16);
    }

    public static Sprite player_right_1(int color) {
        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 1, 1, color, SpriteSheet.tiles, 11, 16);
    }

    public static Sprite player_right_2(int color) {
        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 1, 2, color, SpriteSheet.tiles, 12, 16);
    }

    public static Sprite player_dead1(int color) {
        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 4, 2, color, SpriteSheet.tiles, 14, 16);
    }

    public static Sprite player_dead2(int color) {
        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 5, 2, color, SpriteSheet.tiles, 13, 15);
    }

    public static Sprite player_dead3(int color) {
        return new Sprite(ConstGame.InfoObject.DEFAULT_SIZE, 6, 2, color, SpriteSheet.tiles, 16, 16);
    }

    public static Sprite movingSprite(Sprite normal, Sprite x1, Sprite x2, int time) {
        int diff = time % 3;
        if (diff == 1) {
            return normal;
        }
        if (diff == 2) {
            return x1;
        }
        return x2;
    }

    public static Sprite movingSprite(Sprite normal, Sprite x1, Sprite x2, Sprite x3, int time) {
        int diff = time % 4;
        if (diff == 1) {
            return normal;
        }
        if (diff == 2) {
            return x1;
        }
        if (diff == 3)
            return x2;

        return x3;
    }

    public static Sprite movingSprite(Sprite x1, Sprite x2, int time) {
        return (time % 2 == 1) ? x1 : x2;
    }

    /**
     * hàm phóng to hình ảnh.
     *
     * @param input       hình ảnh cần phóng to.
     * @param scaleFactor số lần phóng(số nguyên dương).
     * @return hình ảnh sau khi phóng to.
     */
    public static Image resample(Image input, int scaleFactor) {
        final int W = (int) input.getWidth();
        final int H = (int) input.getHeight();
        final int S = scaleFactor;

        WritableImage output = new WritableImage(
                W * S,
                H * S);

        PixelReader reader = input.getPixelReader();
        PixelWriter writer = output.getPixelWriter();

        for (int y = 0; y < H; y++) {
            for (int x = 0; x < W; x++) {
                final int argb = reader.getArgb(x, y);
                for (int dy = 0; dy < S; dy++) {
                    for (int dx = 0; dx < S; dx++) {
                        writer.setArgb(x * S + dx, y * S + dy, argb);
                    }
                }
            }
        }

        return output;
    }

    private static int numSpriteInArr(int numSkin, int huong, int num) {
        return numSkin * 12 + huong * 3 + num-1;
    }

    private void setColor(int color) {
        Arrays.fill(_pixels, color);
    }

    private void load() {
        for (int y = 0; y < SIZE; y++) {
            for (int x = 0; x < SIZE; x++) {
                _pixels[x + y * SIZE] = _sheet._pixels[(x + _x) + (y + _y) * _sheet.SIZE];
            }
        }
    }

    private void load(int color) {
        for (int y = 0; y < SIZE; y++) {
            for (int x = 0; x < SIZE; x++) {
                if (_sheet._pixels[(x + _x) + (y + _y) * _sheet.SIZE] == ConstGame.InfoObject.PLAYER_COLOR_GOC) {
                    _pixels[x + y * SIZE] = color;
                } else {
                    _pixels[x + y * SIZE] = _sheet._pixels[(x + _x) + (y + _y) * _sheet.SIZE];
                }
            }
        }
    }

    public int getSize() {
        return SIZE;
    }

    public int[] get_pixels() {
        return _pixels;
    }

    public Image getFxImage() {
        WritableImage wr = new WritableImage(SIZE, SIZE);
        PixelWriter pw = wr.getPixelWriter();
        for (int x = 0; x < SIZE; x++) {
            for (int y = 0; y < SIZE; y++) {
                if (_pixels[x + y * SIZE] == ConstGame.InfoObject.TRANSPARENT_COLOR) {
                    pw.setArgb(x, y, 0);
                } else {
                    pw.setArgb(x, y, _pixels[x + y * SIZE]);
                }
            }
        }
        Image input = new ImageView(wr).getImage();
        return resample(input, ConstGame.InfoObject.SCALED_SIZE / ConstGame.InfoObject.DEFAULT_SIZE);
    }

    public Image getFxImage(int width) {
        WritableImage wr = new WritableImage(SIZE, SIZE);
        PixelWriter pw = wr.getPixelWriter();
        for (int x = 0; x < SIZE; x++) {
            for (int y = 0; y < SIZE; y++) {
                if (_pixels[x + y * SIZE] == ConstGame.InfoObject.TRANSPARENT_COLOR) {
                    pw.setArgb(x, y, 0);
                } else {
                    pw.setArgb(x, y, _pixels[x + y * SIZE]);
                }
            }
        }
        Image input = new ImageView(wr).getImage();
        return resample(input, width / ConstGame.InfoObject.DEFAULT_SIZE);
    }
}
