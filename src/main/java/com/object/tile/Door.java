package com.object.tile;

import com.graphics.ScreenGame;
import com.graphics.Sprite;
import com.hs.ConstGame;
import com.map.Coordinates;
import com.object.character.player.Player;

public class Door extends Tile {

    public Door(float x, float y) {
        super(x, y, Sprite.teleport);
    }


    @Override
    public void render(ScreenGame screenGame) {
        super.render(screenGame);
        screenGame.drawMiniMap(getX(), getY(), ConstGame.MiniMapColor.TELEPORT);
    }

    public boolean check(Player player) {
        return (Coordinates.check2(player, this));
    }
}
