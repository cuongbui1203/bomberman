package com.object.character.enemy.ai;

public class AiLow extends Ai {
    @Override
    public int calculateMove() {
        return Math.abs(random.nextInt()) % 4 + 1;
    }
    @Override
    public int calculateMove(float x, float y, float playerX, float playerY) {
        return calculateMove();
    }
}
