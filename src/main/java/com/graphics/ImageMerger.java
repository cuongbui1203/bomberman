package com.graphics;

import com.hs.ConstGame;
import com.object.Base;
import com.object.tile.Brick;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;

import java.util.List;

/**
 * class tạo hình ảnh tĩnh.
 */
public class ImageMerger {
    /**
     * Gộp các Sprite(hình ảnh) của map tĩnh.
     *
     * @param sprites mảng các hình ảnh cần render.
     * @return một hình ảnh tĩnh đã được gộp chung vào nhau.
     */
    public static Image createStaticImage(Sprite[][] sprites) {
        int r = sprites.length;
        int c = sprites[0].length;
        int size = ConstGame.InfoObject.DEFAULT_SIZE;
        WritableImage wr = new WritableImage(
                size * c,
                size * r);

        PixelWriter pw = wr.getPixelWriter();

        for (int i = 0; i < r; i++) {
            for (int t = 0; t < c; t++) {
                for (int x = 0; x < ConstGame.InfoObject.DEFAULT_SIZE; x++) {
                    for (int y = 0; y < ConstGame.InfoObject.DEFAULT_SIZE; y++) {
                        if (sprites[i][t]._pixels[x + y * size] == ConstGame.InfoObject.TRANSPARENT_COLOR) {
                            pw.setArgb(x + t * size, y + i * size, 0);
                        } else {
                            pw.setArgb(x + t * size, y + i * size, sprites[i][t]._pixels[x + y * ConstGame.InfoObject.DEFAULT_SIZE]);
                        }
                    }
                }
            }
        }

        Image input = new ImageView(wr).getImage();
//        return input;
        return Sprite.resample(input, ConstGame.InfoObject.SCALED_SIZE / ConstGame.InfoObject.DEFAULT_SIZE);
    }


    /**
     * tạo lớp đầu tiên cho render.
     *
     * @param obj mảng các vật thể cần vẽ.
     * @param w   Chiều rộng(của map).
     * @param h   Chiều cao(của map).
     * @return trả về hỉnh ảnh sau khi gộp(đã phóng to).<p>Có thể render ra canvas(màn hình) được luôn.</p>
     */
    public static Image createStaticImage3(List<Brick> obj, int w, int h) {
        int r = h;
        int c = w;
        int size = ConstGame.InfoObject.DEFAULT_SIZE;
        WritableImage wr = new WritableImage(
                size * r,
                size * c);

        PixelWriter pw = wr.getPixelWriter();

        for (int i = 0; i < r * size; i++) {
            for (int t = 0; t < c * size; t++) {
                pw.setArgb(i, t, 0);
            }
        }

        for (Base a : obj) {
            for (int i = 0; i < size; i++) {
                for (int t = 0; t < size; t++) {
                    pw.setArgb((int) (a.getX() * size) + t,
                            (int) (a.getY() * size) + i,
                            a.getSprite()._pixels[t + i * size]);
                }
            }
        }

        return Sprite.resample(new ImageView(wr).getImage(),
                ConstGame.InfoObject.SCALED_SIZE / ConstGame.InfoObject.DEFAULT_SIZE);

    }

}
