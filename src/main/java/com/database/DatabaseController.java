package com.database;

import com.hs.ConstGame;

import java.sql.*;

public class DatabaseController {
    private static final String dbname = "gamesave";
    private static final String c1 = "id";
    private static final String c2 = "name";
    private static final String c3 = "time";
    private static final String c4 = "score";
    private static Connection dbConnect;
    private static String qwerty = "";

    public DatabaseController() {
    }


    public static void init() {
        try {
//            String url = "jdbc:sqlite:E:\\!!!hoc_tap\\lap_Trinh\\java\\DGUI\\database\\dict_hh.db";
            dbConnect = DriverManager.getConnection(ConstGame.InfoGame.PATH_DATABASE);
            System.out.println("Connected to Database");
        } catch (SQLException troubles) {
            System.out.println(troubles.getMessage());
        }
    }

    public static void main(String[] args) {
        addToDatabase("Cường", "05/12/2021", 1000);
        System.out.println(getHighScoreList());
    }

    public static void addToDatabase(String name, String time, int score) {
        init();
        qwerty = "INSERT INTO " + dbname + "(" + c2 + "," + c3 + "," + c4 + ")" + " VALUES (?,?,?);";
        try {
            PreparedStatement pstmt = dbConnect.prepareStatement(qwerty);
            pstmt.setString(1, name);
            pstmt.setString(2, time);
            pstmt.setString(3, String.valueOf(score));
            pstmt.executeUpdate();
            System.out.println("Thêm Thành Công!");
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        } finally {
            close();
        }
    }

    public static String getHighScoreList() {
        init();
        int stt = 1;
        qwerty = "SELECT " + c2 + "," + c3 + "," + c4 + " FROM " + dbname + " ORDER BY "+c4+" DESC;";
        try {
            Statement stmt = dbConnect.createStatement();
            ResultSet res = stmt.executeQuery(qwerty);
            StringBuilder out = new StringBuilder();

            out.append("<html>" +
                    "<meta charset=\"UTF-8\" />" +
                    "<style>\n" +

                    "            table,\n" +
                    "            th,\n" +
                    "            table {\n" +
                    "                width: 100%;\n" +
                    "               font-size:35px;\n" +
                    "border-collapse: collapse;" +
                    "            }\n" +
                    "  \n" +
                    "            table.fixed {\n" +
                    "                table-layout: fixed;\n" +
                    "            }\n" +
                    "            table.fixed td {\n" +
                    "                overflow: hidden;\n" +
                    "            }\n" +
                    "        </style>\n" +
                    "    </head>" +
                    "<body><center>" +
                    "<div style=\"overflow-x: auto;\">" +
                    "<table>" +
                    "<tr style=\"border: 1px solid black;\">\n" +
                    "       <th style=\"width: 10%;text-align: center;border: 1px solid black;\">STT</th>\n" +
                    "       <th style=\"width: 45%;text-align: center;border: 1px solid black;\">Name</th>\n" +
                    "       <th style=\"width: 15%;text-align: center;border: 1px solid black;\">Score</th>\n" +
                    "       <th style=\"width: 30%;text-align: center;border: 1px solid black;\">Time</th>\n" +
                    "</tr>\n"
            );
            String format = "<tr style=\"border: 1px solid black;text-align: center;\"> " +
                    "<td style=\"border: 1px solid black;\">%d</td>" +
                    "<td style=\"border: 1px solid black;\">%s</td>" +
                    "<td style=\"border: 1px solid black;\">%d</td>" +
                    "<td style=\"border: 1px solid black;\">%s</td>" +
                    " </tr>";
            while (res.next()) {
                out.append(String.format(format, stt++, res.getString(c2), res.getInt(c4), res.getString(c3)));
            }
            out.append("</table></div></center></body></html>");
            System.out.println("Lấy dữ liệu thành công!");
            return out.toString();
        } catch (SQLException throwable) {
            throwable.printStackTrace();
            return null;
        } finally {
            close();
        }
    }

    public static void close() {
        try {
            dbConnect.close();
            System.out.println("Disconnected to Database");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

}
