package com.gui.dialog;

import com.graphics.Sprite;
import com.gui.GuiController;
import com.sound.Sound;
import javafx.animation.AnimationTimer;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.util.concurrent.atomic.AtomicReference;

public class AlertSetting {

    public static void show(Sound sound, GuiController gui, AnimationTimer timer) {

        AtomicReference<Double> lastBgm = new AtomicReference<>(sound.getBgmVolume());
        AtomicReference<Double> lastSound = new AtomicReference<>(sound.getSoundVolume());

        Alert alert = new Alert(Alert.AlertType.NONE);

        Stage stageAlert = (Stage) alert.getDialogPane().getScene().getWindow();
        stageAlert.getIcons().add(Sprite.icon_1);

        alert.setTitle("Cài Đặt");
        alert.setHeaderText("Cài đặt các thông số của game");

        ButtonType btn = new ButtonType("Xác Nhận", ButtonBar.ButtonData.OK_DONE);
        ButtonType btn2 = new ButtonType("Menu", ButtonBar.ButtonData.OK_DONE);
//        ToggleButton btn3 = btn2;


        alert.getDialogPane().getButtonTypes().addAll(btn, btn2);


        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(10));
        CheckBox bgmMute = new CheckBox("Mute");
        CheckBox soundMute = new CheckBox("Mute");

        Slider bgmVolume = new Slider();
        bgmVolume.setMin(0);
        bgmVolume.setMax(100);
        bgmVolume.setValue(sound.getBgmVolume() * 100);
        bgmVolume.valueProperty().addListener((observable, oldValue, newValue) -> {
            sound.setBgmVolume(newValue.doubleValue() / 100);
            bgmMute.setSelected(newValue.doubleValue() == 0);
            if (!bgmMute.isSelected()) {
                lastBgm.set(sound.getBgmVolume());
            }
        });

        bgmMute.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                bgmVolume.setValue(0);
            } else {
                bgmVolume.setValue(lastBgm.get() * 100);
            }
        });

        Slider soundVolume = new Slider();
        soundVolume.setMax(100);
        soundVolume.setMin(0);
        soundVolume.setValue(sound.getSoundVolume() * 100);
        soundVolume.valueProperty().addListener((observable, oldValue, newValue) -> {
            sound.setSoundVolume(newValue.doubleValue() / 100);
            soundMute.setSelected(newValue.doubleValue() == 0);
            if (!soundMute.isSelected())
                lastSound.set(sound.getSoundVolume());
        });

        soundMute.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) soundVolume.setValue(0);
            else soundVolume.setValue(lastSound.get() * 100);
        });

        grid.add(new Label("Nhạc Nền:"), 0, 0);
        grid.add(bgmVolume, 1, 0);
        grid.add(new Label("Âm Thanh:"), 0, 1);
        grid.add(soundVolume, 1, 1);
        grid.add(bgmMute, 2, 0);
        grid.add(soundMute, 2, 1);

        alert.getDialogPane().setContent(grid);

        alert.show();

        alert.setResultConverter(param -> {
            if (timer != null)
                timer.start();
            if (param == btn2) {
                gui.goHome();
            }
            return param;
        });

    }

}
