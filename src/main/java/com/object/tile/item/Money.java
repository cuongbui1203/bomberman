package com.object.tile.item;

import com.graphics.Sprite;

public class Money extends Item{
    public Money(float x, float y,int id) {
        super(x, y, (id == 1? Sprite.bac:(id==2? Sprite.vang:Sprite.kc)));
        this.id = id;
    }

    public void load(int score){
        switch (id){
            case 1:
                score += 250;
                break;
            case 2:
                score += 500;
                break;
            case 3:
                score += 1000;
                break;
        }
    }

    public int getScore(){
        if(id == 1) return 250;
        if(id == 2) return 500;
        return 1000;
    }

    @Override
    protected void load() {

    }
}
