package com.socket.sever;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Sever {

    final int port;
    private Socket socket;
    private ServerSocket serverSocket;
    private DataOutputStream dos;
    private DataInputStream dis;
    private boolean contented = false;

    public Sever(int port) {
        this.port = port;
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            System.out.println("Exception in constructor Sever");
            System.exit(-2);
        }
    }

    public void connect() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    socket = serverSocket.accept();
                    contented = true;
                    dos = new DataOutputStream(socket.getOutputStream());
                    dis = new DataInputStream(socket.getInputStream());

                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        });

        t.start();

    }

    public void sendData(String data) {
        try {
            dos.writeUTF(data);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String reserveData() {
        String res = "null";
        try {
            res = dis.readUTF();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return res;
    }

    public void close() {
        try {
            socket.close();
            serverSocket.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static class SeverHandel implements Runnable {

        final DataInputStream dis;
        final DataOutputStream dos;
        Socket s;

        private SeverHandel(Socket s, DataInputStream dis, DataOutputStream dos) {
            this.dis = dis;
            this.dos = dos;
            this.s = s;
        }

        @Override
        public void run() {
            String r = "";
            while (true) {
                try {
                    r = dis.readUTF();
                    System.out.println(r);
                    if (r.equals("end")) {
                        dos.writeUTF("end");
                        return;
                    }
                    dos.writeUTF("Sever>>" + r);
                    if (s.isClosed())
                        break;
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    public static void main(String[] args) {
        int port = 65520;
        while (true) {
            try {

                ServerSocket serverSocket = new ServerSocket(port);
                Socket socket = serverSocket.accept();
                DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
                DataInputStream dis = new DataInputStream(socket.getInputStream());
                SeverHandel severHandel = new SeverHandel(socket, dis, dos);
                Thread thread = new Thread(severHandel);
                thread.start();

            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
