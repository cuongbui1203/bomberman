package com.object.character.player;

import com.graphics.ScreenGame;
import com.graphics.Sprite;
import com.hs.ConstGame;
import com.input.Keyboard2;
import com.map.Coordinates;
import com.object.character.Character;
import com.object.character.enemy.Enemy;
import com.object.tile.item.Item;
import com.object.tile.item.Portal;

import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

public abstract class Player extends Character {
    protected float xOffset;
    protected float yOffset;
    protected Keyboard2 input;
    protected float speedBonus;
    protected int flameSegmentBonus;
    protected int bombBonus;
    protected boolean wallPass;
    protected Timer timer;
    protected int timeWallPass = ConstGame.InfoObject.WALL_PASS_TIME;
    protected int color = ConstGame.InfoObject.SELECTED_PLAYER_COLOR_RED;
    protected Boolean sex; // nam : nu
    protected String name;
    protected int heat;

    protected int id;

    //    protected
    public Player(int id, float x, float y, Boolean sex, String name) {
        super(x, y);
        this.id = id;
        xOffset = 0;
        yOffset = 0;
        this.sex = sex;
        this.name = name;
        heat = 3;
    }
    public Player(int id,float x, float y, boolean sex, String name) {
        super(x, y);
        this.id = id;
        xOffset = 0;
        yOffset = 0;
        this.sex = sex;
        this.name = name;
        heat = 3;
    }

    public int getId() {
        return id;
    }

    @Override
    public void kill() {
        if (t == 0) {
            heat--;
            alive = false;
        }
    }

    @Override
    public boolean isDie() {
        if (heat > 0 && super.isDie()) {
            reborn();
            return false;
        } else {
            return super.isDie();
        }
    }

    @Override
    protected void load() {
        if(Objects.isNull(sex)){
            FRAME[1] = Sprite.player_up_skin(color);
            FRAME[2] = Sprite.player_up_1_skin(color);
            FRAME[3] = Sprite.player_up_2_skin(color);
            FRAME[4] = Sprite.player_right_skin(color);
            FRAME[5] = Sprite.player_right_1_skin(color);
            FRAME[6] = Sprite.player_right_2_skin(color);
            FRAME[7] = Sprite.player_left_skin(color);
            FRAME[8] = Sprite.player_left_1_skin(color);
            FRAME[9] = Sprite.player_left_2_skin(color);
            FRAME[10] = Sprite.player_down_skin(color);
            FRAME[11] = Sprite.player_down_1_skin(color);
            FRAME[12] = Sprite.player_down_2_skin(color);
        }else if (sex) {
            FRAME[1] = Sprite.player_up(color);
            FRAME[2] = Sprite.player_up_1(color);
            FRAME[3] = Sprite.player_up_2(color);
            FRAME[4] = Sprite.player_right(color);
            FRAME[5] = Sprite.player_right_1(color);
            FRAME[6] = Sprite.player_right_2(color);
            FRAME[7] = Sprite.player_left(color);
            FRAME[8] = Sprite.player_left_1(color);
            FRAME[9] = Sprite.player_left_2(color);
            FRAME[10] = Sprite.player_down(color);
            FRAME[11] = Sprite.player_down_1(color);
            FRAME[12] = Sprite.player_down_2(color);
        } else {
            FRAME[1] = Sprite.player_nu_up(color);
            FRAME[2] = Sprite.player_nu_up_1(color);
            FRAME[3] = Sprite.player_nu_up_2(color);
            FRAME[4] = Sprite.player_nu_right(color);
            FRAME[5] = Sprite.player_nu_right_1(color);
            FRAME[6] = Sprite.player_nu_right_2(color);
            FRAME[7] = Sprite.player_nu_left(color);
            FRAME[8] = Sprite.player_nu_left_1(color);
            FRAME[9] = Sprite.player_nu_left_2(color);
            FRAME[10] = Sprite.player_nu_down(color);
            FRAME[11] = Sprite.player_nu_down_1(color);
            FRAME[12] = Sprite.player_nu_down_2(color);
        }
        FRAME[13] = Sprite.player_dead1(color);
        FRAME[14] = Sprite.player_dead2(color);
        FRAME[15] = Sprite.player_dead3(color);

    }

    public double getXOffset() {
        return xOffset;
    }

    public double getYOffset() {
        return yOffset;
    }

    public void setInput(Keyboard2 input) {
        this.input = input;
    }

    @Override
    public float getX() {
        return super.getX() + xOffset;
    }

    public void setX(float x) {
        float xc = mapViewW / 2;
        if (x <= xc) {
            this.x = x;
            xOffset = 0;
        } else if (x - xc <= mapW - mapViewW) {
            this.x = xc;
            xOffset = x - this.x;
        } else {
            xOffset = mapW - mapViewW;
            this.x = x - xOffset;
        }
    }

    @Override
    public float getY() {
        return super.getY() + yOffset;
    }

    public void setY(float y) {
        float yc = mapViewH / 2;
        if (y <= yc) {
            this.y = y;
            yOffset = 0;
        } else if (y - yc <= mapH - mapViewH) {
            this.y = yc;
            yOffset = y - this.y;
        } else {
            yOffset = mapH - mapViewH;
            this.y = y - yOffset;
        }

    }

    public void updateItem(Item item) {
        if(item instanceof Portal) return;
        speedBonus += item.getSpeedExchange();
        bombBonus += item.getBombExchange();
        flameSegmentBonus += item.getFlameExchange();
        if (item.isWallPass()) starTimerWallPass();
    }

    public int getFlameSegmentBonus() {
        return flameSegmentBonus;
    }

    public int getNumBomb() {
        return bombBonus;
    }

    @Override
    public double getSpeed() {
        return super.getSpeed() + speedBonus;
    }

    public void setSpeed(long now) {
        speed = now;
    }

    @Override
    public void calculateMove() {
    }

    /**
     * hàm render các hình ảnh cảu nhân vật.
     *
     * @param screenGame màn hình cần render.
     * @param now        số thứ tự khung hình hiển thị.
     */
    public void render(ScreenGame screenGame, int now) {
        if (alive) {
            if (moving) {
                switch (dir) {
                    case ConstGame.Direction.UP:
                        screenGame.drawSprite(Sprite.movingSprite(FRAME[2], FRAME[3], now), getX(), getY());
                        break;
                    case ConstGame.Direction.DOWN:
                        screenGame.drawSprite(Sprite.movingSprite(FRAME[11], FRAME[12], now), getX(), getY());
                        break;
                    case ConstGame.Direction.LEFT:
                        screenGame.drawSprite(Sprite.movingSprite(FRAME[8], FRAME[9], now), getX(), getY());
                        break;
                    case ConstGame.Direction.RIGHT:
                        screenGame.drawSprite(Sprite.movingSprite(FRAME[5], FRAME[6], now), getX(), getY());
                        break;
                }
            } else {

                switch (dir) {
                    case ConstGame.Direction.UP:
                        screenGame.drawSprite(FRAME[1], getX(), getY());
                        break;
                    case ConstGame.Direction.DOWN:
                        screenGame.drawSprite(FRAME[10], getX(), getY());
                        break;
                    case ConstGame.Direction.LEFT:
                        screenGame.drawSprite(FRAME[7], getX(), getY());
                        break;
                    case ConstGame.Direction.RIGHT:
                        screenGame.drawSprite(FRAME[4], getX(), getY());
                        break;
                }
            }
        } else {
            if (old != now) {
                t++;
                old = now;
            }
            screenGame.drawSprite(Sprite.movingSprite(FRAME[13],
                    FRAME[14], FRAME[15], t), getX(), getY());
        }
        screenGame.drawMiniMap(getX(), getY(), ConstGame.MiniMapColor.PLAYER);
    }

    @Override
    public void checkEnemy(List<Enemy> enemies) {
        for (Enemy e : enemies) {
            if (Coordinates.checkE(this, e)) {
                kill();
                break;
            }
        }
    }


    @Override
    public boolean checkMap(int[][] temp) {

        if (wallPass) {
            setSpeed(ConstGame.InfoObject.SPEED_CHARACTER);
        } else if (Coordinates.check2((Bomber) this, temp)) {
            setSpeed(0);
        } else setSpeed(ConstGame.InfoObject.SPEED_CHARACTER);


        return false;
//
//        if (Coordinates.check2((Bomber) this, temp) && !wallPass) {
//            setSpeed(0);
//            return true;
//        } else setSpeed(ConstGame.InfoObject.SPEED_CHARACTER);
//        return false;
    }

    public void setTD(float x, float y) {
        setX(x);
        setY(y);
    }

    public void reborn() {
        setTD(1, 1);
        alive = true;
        t = 0;
    }
    public void reborn(int x,int y) {
        setTD(x, y);
        alive = true;
        t = 0;
    }

    private void starTimerWallPass() {
        if (timer == null) {
            timer = new Timer();
        }
        wallPass = true;
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (timeWallPass < 0) {
                    timer.cancel();
                    timeWallPass = ConstGame.InfoObject.WALL_PASS_TIME;
                    wallPass = false;
                } else timeWallPass--;
            }
        }, 0, 1000);
    }

    public String getName() {
        return name;
    }

    public int getHeat() {
        return heat;
    }
}
