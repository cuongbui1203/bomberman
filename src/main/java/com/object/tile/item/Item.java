package com.object.tile.item;

import com.graphics.Sprite;
import com.map.Coordinates;
import com.object.character.player.Bomber;
import com.object.tile.Tile;

public abstract class Item extends Tile {

    protected int bombExchange;
    protected int flameExchange;
    protected float speedExchange;
    protected boolean wallPass;
    protected boolean show;
    protected int id;
    protected boolean done;

    public Item(float x, float y, Sprite sprite) {
        super(x, y, sprite);
        wallPass = false;
        bombExchange = 0;
        flameExchange = 0;
        speedExchange = 0;
        show = false;
        done = false;
        load();
    }

    public Item(float x, float y, Sprite sprite, int id) {
        this(x, y, sprite);
        this.id = id;
    }

    public void update(Bomber player) {
        if (Coordinates.check(player, this)  && !done && show) {
            player.updateItem(this);
            done = true;
        }
    }


    protected abstract void load();

    public int getBombExchange() {
        return bombExchange;
    }

    public int getFlameExchange() {
        return flameExchange;
    }

    public float getSpeedExchange() {
        return speedExchange;
    }

    public boolean isShow() {
        return show && !done;
    }

    public boolean isWallPass() {
        return wallPass;
    }

    public void Show() {
        this.show = true;
    }

    public boolean isDone() {
        return done;
    }
}
