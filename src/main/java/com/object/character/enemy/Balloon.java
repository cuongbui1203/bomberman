package com.object.character.enemy;

import com.graphics.Sprite;
import com.object.character.enemy.ai.AiLow;

public class Balloon extends Enemy {

    public Balloon(float x, float y) {
        super(x, y);
        ai = new AiLow();
        moving = true;
        score = 100;
    }

    @Override
    protected void load() {
        FRAME[1] = Sprite.balloom_left1;
        FRAME[2] = Sprite.balloom_left2;
        FRAME[3] = Sprite.balloom_left3;
        FRAME[4] = Sprite.balloom_right1;
        FRAME[5] = Sprite.balloom_right2;
        FRAME[6] = Sprite.balloom_right3;
        FRAME[7] = Sprite.balloom_dead;
        FRAME[8] = Sprite.mob_dead1;
        FRAME[9] = Sprite.mob_dead2;
        FRAME[10] = Sprite.mob_dead3;
    }

}
