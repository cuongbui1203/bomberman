package com.object.character.player;

import com.hs.ConstGame;

/**
 * class quản lý nhân vật của người chơi.
 */
public class Bomber extends Player {

    public Bomber(int id, float x, float y, int color, Boolean sex, String name) {
        super(id, x, y, sex, name);
        this.color = color;
        System.out.println(color);
        xOffset = 0;
        yOffset = 0;
        speedBonus = 0;
        flameSegmentBonus = 0;
        bombBonus = 1;
        wallPass = false;
        load();
    }



    /**
     * di chuyển theo chiều ngang.
     *
     * @param d chiều chuyển dộng
     */
    private void moveW(int d) {
        if(id == 0) {
            float xc = mapViewW / 2;
            if (d == ConstGame.Direction.RIGHT) {
                if (x < xc) {
                    x += getSpeed();
                    if (x >= xc)
                        x = xc;
                } else if (xOffset < mapW - mapViewW) {
                    xOffset += getSpeed();
                    if (xOffset >= mapW - mapViewW)
                        xOffset = mapW - mapViewW;
                } else if (x < mapViewW) {
                    x += getSpeed();
                    if (x >= mapViewW)
                        x = mapViewW;
                }
            }

            if (d == ConstGame.Direction.LEFT) {

                if (x > xc) {
                    x -= getSpeed();
                    if (x <= xc)
                        x = xc;
                } else if (xOffset > 0) {
                    xOffset -= getSpeed();
                    if (xOffset <= 0)
                        xOffset = 0;
                } else if (x > 0) {
                    x -= getSpeed();
                    if (x <= 0)
                        x = 0;
                }
            }
        } else {
            if(d == ConstGame.Direction.RIGHT && x + 32 * getSpeed() < xOffset + mapViewW) x += getSpeed();
            else if(d == ConstGame.Direction.LEFT && x > xOffset) x -= getSpeed();
        }
    }

    /**
     * di chuyển theo chiều dọc.
     *
     * @param d chiều chuyển dộng
     */
    private void moveH(int d) {
        if(id == 0) {
            float yc = mapViewH / 2;

            if (d == ConstGame.Direction.DOWN) {

                if (y < yc) {
                    y += getSpeed();
                    if (y >= yc)
                        y = yc;
                } else if (yOffset < mapH - mapViewH) {
                    yOffset += getSpeed();
                    if (yOffset >= mapH - mapViewH)
                        yOffset = mapH - mapViewH;
                } else if (y < mapViewH) {
                    y += getSpeed();
                    if (y >= mapViewH)
                        y = mapViewH;
                }

            }

            if (d == ConstGame.Direction.UP) {
                if (y > yc) {
                    y -= getSpeed();
                    if (y <= yc)
                        y = yc;
                } else if (yOffset > 0) {
                    yOffset -= getSpeed();
                    if (yOffset <= 0)
                        yOffset = 0;
                } else if (y > 0) {
                    y -= getSpeed();
                    if (y <= 0)
                        y = 0;
                }
            }
        } else {
//            System.out.println(x + " " + y);
            if(d == ConstGame.Direction.DOWN && y + 32 * getSpeed() < yOffset + mapViewH) y += getSpeed();
            else if(d == ConstGame.Direction.UP && y > yOffset) y -= getSpeed();
        }
    }


    @Override
    public void move() {
        if (moving) {
            switch (dir) {
                case ConstGame.Direction.LEFT:
                case ConstGame.Direction.RIGHT: {
                    if (Math.abs(getY() - (int) getY()) < 0.1)
                        moveW(dir);
                    else if (getY() > ((int) getY()) + 0.5)
                        moveH(ConstGame.Direction.DOWN, (int) getY());
                    else
                        moveH(ConstGame.Direction.UP, (int) getY());
                    break;
                }
                case ConstGame.Direction.DOWN:
                case ConstGame.Direction.UP: {
                    if (Math.abs(getX() - (int) getX()) < 0.1)
                        moveH(dir);
                    else if (getX() > ((int) getX() + 0.5))
                        moveW(ConstGame.Direction.RIGHT, (int) getX());
                    else
                        moveW(ConstGame.Direction.LEFT, (int) getX());
                    break;
                }
            }
        }
    }

    @Override
    public void update() {
        /*if(id == 1) {
            ((Keyboard2)input).reloadKey();
        }*/
        if (input.Up.get(id))
            dir = ConstGame.Direction.UP;
        if (input.Down.get(id))
            dir = ConstGame.Direction.DOWN;
        if (input.Left.get(id))
            dir = ConstGame.Direction.LEFT;
        if (input.Right.get(id))
            dir = ConstGame.Direction.RIGHT;

        moving = (input.Up.get(id) || input.Down.get(id) || input.Left.get(id) || input.Right.get(id));
        if (alive)
            move();
    }

    private void moveW(int d, int endX) {
        double xc = mapViewW / 2;

        if (d == ConstGame.Direction.RIGHT) {
//            endX++;
            if (getX() < endX) {
                if (getX() <= endX) {
                    if (endX < xc) {
                        x += getSpeed();
                        if (x >= endX) {
                            x = endX;
                        }
                    } else if (endX < mapW - mapViewW) {
                        xOffset += getSpeed();
                        if (getX() >= endX) {
                            xOffset = endX - x;
                        }
                    } else if (endX < mapW) {
                        x += getSpeed();
                        if (getX() >= endX) {
                            x = endX - xOffset;
                        }
                    }
                }
            }
        }


        if (d == ConstGame.Direction.LEFT) {
//            endX--;
            if (getX() > endX) {
                if (endX > xc + mapViewW) {
                    x -= getSpeed();
                    if (x <= endX)
                        x = endX - xOffset;
                } else if (endX > xc) {
                    xOffset -= getSpeed();
                    if (getX() <= endX)
                        xOffset = (float) (endX - xc);
                } else if (endX > 1) {
                    x -= getSpeed();
                    if (getX() <= endX)
                        x = endX;
                }
            }
        }
    }

    private void moveH(int d, int endY) {
        double yc = mapViewH / 2;
        if (d == ConstGame.Direction.DOWN) {
            endY++;
            if (getY() <= endY) {
                if (endY < yc) {
                    y += getSpeed();
                    if (getY() >= endY) {
                        y = endY;
                    }
                } else if (endY < mapW - mapViewW) {
                    yOffset += getSpeed();
                    if (getY() >= endY) {
                        yOffset = endY - y;
                    }
                } else if (endY < mapW) {
                    y += getSpeed();
                    if (getY() >= endY) {
                        y = endY - yOffset;
                    }
                }
            }

        }

        if (d == ConstGame.Direction.UP) {
            endY--;
            if (endY < 0) {
                endY = 0;
            }
            if (getY() >= endY) {
                if (endY > yc + mapViewH) {
                    y -= getSpeed();
                    if (getY() <= endY)
                        y = endY - mapViewH;
                } else if (endY > yc) {
                    yOffset -= getSpeed();
                    if (getY() <= endY)
                        yOffset = endY - y;
                } else if (endY > 1) {
                    y -= getSpeed();
                    if (getY() < endY)
                        y = endY;
                }
            }
        }
    }

}
