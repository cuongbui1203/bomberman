package com.object.bom.flame;

import com.graphics.ScreenGame;
import com.graphics.Sprite;
import com.map.Coordinates;
import com.object.Base;
import com.object.character.Character;
import javafx.scene.effect.Light;

import java.util.ArrayList;
import java.util.List;

public class FlameSegment extends Base {

    private static final Sprite[] FLAMES = {
            // 0
            Sprite.bomb_exploded,
            Sprite.explosion_horizontal,
            Sprite.explosion_horizontal_left_last,
            Sprite.explosion_horizontal_right_last,
            Sprite.explosion_vertical,
            Sprite.explosion_vertical_down_last,
            Sprite.explosion_vertical_top_last,
            // 7
            Sprite.bomb_exploded1,
            Sprite.explosion_horizontal1,
            Sprite.explosion_horizontal_left_last1,
            Sprite.explosion_horizontal_right_last1,
            Sprite.explosion_vertical1,
            Sprite.explosion_vertical_down_last1,
            Sprite.explosion_vertical_top_last1,
            // 14
            Sprite.bomb_exploded2,
            Sprite.explosion_horizontal2,
            Sprite.explosion_horizontal_left_last2,
            Sprite.explosion_horizontal_right_last2,
            Sprite.explosion_vertical2,
            Sprite.explosion_vertical_down_last2,
            Sprite.explosion_vertical_top_last2
    };

    private final List<Integer> upLength = new ArrayList<>();
    private final List<Integer> downLength = new ArrayList<>();
    private final List<Integer> leftLength = new ArrayList<>();
    private final List<Integer> rightLength = new ArrayList<>();
    private final List<Integer> connectFlame = new ArrayList<>();

    private final List<Light.Point> centerPoint = new ArrayList<>();

    public FlameSegment(float x, float y) {
        super(x, y, null);
        centerPoint.add(new Light.Point());
        centerPoint.get(0).setX(x);
        centerPoint.get(0).setY(y);
    }


    public void update(int up, int right, int down, int left) {
        upLength.add(up);
        downLength.add(down);
        leftLength.add(left);
        rightLength.add(right);
    }

    public void update(List<List<Integer>> length) {
        upLength.addAll(length.get(0));
        rightLength.addAll(length.get(1));
        downLength.addAll(length.get(2));
        leftLength.addAll(length.get(3));
    }

    public void addCenterPoint(float x, float y) {
        centerPoint.add(new Light.Point());
        centerPoint.get(centerPoint.size() - 1).setX(x);
        centerPoint.get(centerPoint.size() - 1).setY(y);
    }

    @Override
    public void update() {
    }

    @Override
    public void render(ScreenGame screenGame, int now) {
        for (int t = 0; t < centerPoint.size(); t++) {
            // trung tâm
            screenGame.drawSprite(Sprite.movingSprite(FlameSegment.FLAMES[0],
                            FlameSegment.FLAMES[7],
                            FlameSegment.FLAMES[14], now)
                    , centerPoint.get(t).getX(), centerPoint.get(t).getY());

            // bên trái
            if (leftLength.get(t) > 0) {
                for (int i = 1; i <= leftLength.get(t) - 1; i++) {
                    screenGame.drawSprite(Sprite.movingSprite(FlameSegment.FLAMES[1],
                                    FlameSegment.FLAMES[8],
                                    FlameSegment.FLAMES[15], now)
                            , centerPoint.get(t).getX() - i, centerPoint.get(t).getY());
                }
                screenGame.drawSprite(Sprite.movingSprite(FlameSegment.FLAMES[2],
                                FlameSegment.FLAMES[9],
                                FlameSegment.FLAMES[16], now)
                        , centerPoint.get(t).getX() - leftLength.get(t), centerPoint.get(t).getY());

            }

            // bên phải
            if (rightLength.get(t) > 0) {
                for (int i = 1; i <= rightLength.get(t) - 1; i++) {
                    screenGame.drawSprite(Sprite.movingSprite(FlameSegment.FLAMES[1],
                                    FlameSegment.FLAMES[8],
                                    FlameSegment.FLAMES[15], now)
                            , centerPoint.get(t).getX() + i, centerPoint.get(t).getY());
                }
                screenGame.drawSprite(Sprite.movingSprite(FlameSegment.FLAMES[3],
                                FlameSegment.FLAMES[10],
                                FlameSegment.FLAMES[17], now)
                        , centerPoint.get(t).getX() + rightLength.get(t), centerPoint.get(t).getY());
            }
            // bên dưới
            if (downLength.get(t) > 0) {
                for (int i = 1; i <= downLength.get(t) - 1; i++) {
                    screenGame.drawSprite(Sprite.movingSprite(FlameSegment.FLAMES[4],
                                    FlameSegment.FLAMES[11],
                                    FlameSegment.FLAMES[18], now)
                            , centerPoint.get(t).getX(), centerPoint.get(t).getY() + i);
                }
                screenGame.drawSprite(Sprite.movingSprite(FlameSegment.FLAMES[5],
                                FlameSegment.FLAMES[12],
                                FlameSegment.FLAMES[19], now)
                        , centerPoint.get(t).getX(), centerPoint.get(t).getY() + downLength.get(t));
            }
            // bên trên
            if (upLength.get(t) > 0) {
                for (int i = 1; i <= upLength.get(t) - 1; i++) {
                    screenGame.drawSprite(Sprite.movingSprite(FlameSegment.FLAMES[4],
                                    FlameSegment.FLAMES[11],
                                    FlameSegment.FLAMES[18], now)
                            , centerPoint.get(t).getX(), centerPoint.get(t).getY() - i);
                }
                screenGame.drawSprite(Sprite.movingSprite(FlameSegment.FLAMES[6],
                                FlameSegment.FLAMES[13],
                                FlameSegment.FLAMES[20], now)
                        , centerPoint.get(t).getX(), centerPoint.get(t).getY() - upLength.get(t));
            }
        }
    }

    private void calculateFlameConnect() {
        for (int i = 1; i < centerPoint.size(); i++) {
            if (centerPoint.get(0).getX() == centerPoint.get(i).getX()) {
                connectFlame.add(i);
                connectFlame.add(1);
                connectFlame.add((int) Math.abs(centerPoint.get(0).getY() - centerPoint.get(i).getY()));
                if(centerPoint.get(0).getY() > centerPoint.get(i).getY()){
                    upLength.set(0,-1);
                    downLength.set(i,-1);
                    connectFlame.add(-1);
                }else {
                    downLength.set(0,-1);
                    upLength.set(i,-1);
                    connectFlame.add(1);
                }
            }
            if (centerPoint.get(0).getY() == centerPoint.get(i).getY()) {
                connectFlame.add(i);
                connectFlame.add(-1);
                connectFlame.add((int) Math.abs(centerPoint.get(0).getX() - centerPoint.get(i).getX()));
                if(centerPoint.get(0).getX() > centerPoint.get(i).getX()){
                    leftLength.set(0,-1);
                    rightLength.set(i,-1);
                    connectFlame.add(-1);
                }else {
                    rightLength.set(0,-1);
                    leftLength.set(i,-1);
                    connectFlame.add(1);
                }
            }
        }
    }

    public void attack(Character ano, int score) {
        for (int i = 0; i < centerPoint.size(); i++) {
            if (Coordinates.check(centerPoint.get(i).getX(), centerPoint.get(i).getY() - upLength.get(i), 1.0, upLength.get(i), ano) ||
                    Coordinates.check(centerPoint.get(i).getX(), centerPoint.get(i).getY(), 1.0, downLength.get(i) + 1, ano) ||
                    Coordinates.check(centerPoint.get(i).getX() - leftLength.get(i), centerPoint.get(i).getY(), leftLength.get(i), 1.0, ano) ||
                    Coordinates.check(centerPoint.get(i).getX(), centerPoint.get(i).getY(), rightLength.get(i) + 1.0, 1.0, ano)) {
                ano.kill();
                score+=ano.getScore();
            }
        }
    }

}
