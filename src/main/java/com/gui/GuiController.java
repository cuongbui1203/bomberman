package com.gui;

import com.database.DatabaseController;
import com.graphics.Sprite;
import com.gui.dialog.AlertSetting;
import com.gui.dialog.DialogChaneSettingPlayer;
import com.gui.dialog.MultiPlaySetting;
import com.gui.dialog.WaiToKetNoi;
import com.hs.ConstGame;
import com.input.Keyboard;
import com.input.Keyboard2;
import com.main.MainGame;
import com.main.MainGameMutilatePlayer;
import com.socket.Online;
import com.socket.client.Client;
import com.socket.sever.Sever;
import com.sound.Sound;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/**
 * class diều khiển và hiển thị giao diện.
 */
public class GuiController extends Application {
    private static final Keyboard2 input = new Keyboard2();
    private static final Sound sound = new Sound();
    @FXML
    public Label fpsShow, hdLabel, levelLabel, name, score,score1, lifeCount,lifeCount1,lifeCount1_2,levelLabel1,name1_2,name1;
    private MainGame game;
    private MainGameMutilatePlayer gameMutilatePlayer;
    @FXML
    private AnchorPane gameScreen, homeScreen, HDScreen, highScoreScreen, settingScreen,gameScreen2;
    @FXML
    private Canvas minimap, canvasMainGame,minimap1,canvasMainGame1;
    @FXML
    private WebView highSoreView;
    private WebEngine highScoreEngine;

    private final Online online = new Online();

    public static void main(String[] args) {
        launch();
    }

    @FXML
    public void init() {
        sound.playBgm();
    }


    @FXML
    private boolean initGame() {
        if (game == null) {
            Pair<String, Pair<Integer, Boolean>> playerStatus = DialogChaneSettingPlayer.show();
            if (playerStatus == null) {
                goHome();
                return false;
            } else {
                name.setText(playerStatus.getKey());
                game = new MainGame(canvasMainGame, minimap, score, lifeCount, levelLabel, input, this, playerStatus, sound);
            }
        } else game.restart(true);
        return true;
    }

    @FXML
    public boolean initGameOnline(){
        if (gameMutilatePlayer == null ) {
            input.setAllCS(online);
            //MultiPlaySetting.show(online);
            Pair<Pair<String, Pair<Integer, Boolean>>,Pair<String, Pair<Integer, Boolean>>> playerStatus = DialogChaneSettingPlayer.show2Play();
//            DialogChaneSettingPlayer.show2Play();
            if (playerStatus == null) {
                goHome();
                return false;
            } else {
                Pair<String, Pair<Integer, Boolean>> playerStatus1 = playerStatus.getKey();
                Pair<String, Pair<Integer, Boolean>> playerStatus2 = playerStatus.getValue();
                name1.setText(playerStatus1.getKey());
                name1_2.setText(playerStatus2.getKey());
                gameMutilatePlayer = new MainGameMutilatePlayer(canvasMainGame1, minimap1,
                        score1, lifeCount1,lifeCount1_2, levelLabel1, input, this,
                        playerStatus1,playerStatus2, sound);
            }
        }
//        } else gameMutilatePlayer.restart(true);
        return true;
    }
    @FXML
    public void play() {
        if (initGame()) {
            disableAllScreen();
            gameScreen.setVisible(true);
            game.run();
        }
    }

    @FXML
    public void playOnline(){
        if(initGameOnline()){
            disableAllScreen();
            gameScreen2.setVisible(true);
            gameMutilatePlayer.run();
        }
    }
    @FXML
    public void setting() {
        AlertSetting.show(sound, this, null);
    }

    @FXML
    public void confirmSetting() {
        disableAllScreen();
        homeScreen.setVisible(true);
    }

    @FXML
    public void goHome() {
        disableAllScreen();
        homeScreen.setVisible(true);
        if (game != null)
            game.destroy();
        game = null;
        if(gameMutilatePlayer != null){
            gameMutilatePlayer.destroy();
        }
        gameMutilatePlayer = null;
    }

    @FXML
    public void hdScreen() {
        disableAllScreen();
        HDScreen.setVisible(true);
        try {
            Scanner sc = new Scanner(new File(ConstGame.InfoGame.PATH_HD));

            StringBuilder res = new StringBuilder();

            while (sc.hasNext()) {
                res.append(sc.nextLine()).append("\n");
            }
            hdLabel.setText(res.toString());
            sc.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void highScoreScreen() {
        disableAllScreen();
        if (highScoreEngine == null) {
            highScoreEngine = highSoreView.getEngine();
            highSoreView.setZoom(0.9);
        }

        highScoreScreen.setVisible(true);
        highScoreEngine.loadContent(DatabaseController.getHighScoreList());
    }

    public void disableAllScreen() {
        if (game != null) {
            game.stop();
        }
        fpsShow.setText("");
        homeScreen.setVisible(false);
        gameScreen.setVisible(false);
        gameScreen2.setVisible(false);
        HDScreen.setVisible(false);
        highScoreScreen.setVisible(false);
        settingScreen.setVisible(false);
    }

    @FXML
    public void exit() {
        sound.end();
        System.exit(2);
    }

    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(GuiController.class.getResource(ConstGame.InfoGame.PATH_FXML));
        Scene scene = new Scene(fxmlLoader.load(), 985, 535);

        scene.setOnKeyPressed(input::keyPressed);

        scene.setOnKeyReleased(input::keyReleased);
        stage.getIcons().add(Sprite.icon);
        stage.setTitle(ConstGame.InfoGame.NAME);
        stage.setResizable(false);
        stage.setScene(scene);
        stage.show();

    }

    @Override
    public void stop() {
        sound.end();
        System.exit(1);
//        game.reset();
    }

}
