package com.object.character.enemy;

import com.graphics.ScreenGame;
import com.graphics.Sprite;
import com.hs.ConstGame;
import com.object.character.Character;
import com.object.character.enemy.ai.Ai;
import com.object.character.player.Player;
import com.object.tile.item.Money;

import java.util.Random;

public abstract class Enemy extends Character {
    protected Ai ai;
    protected int endX;
    protected int endY;

    protected Money money = null;

    public Money getMoney() {
        return money;
    }

    public Enemy(float x, float y) {
        super(x, y);
        speed = ConstGame.InfoObject.SPEED_ENEMY;
        old = 1000;
        t = 0;
        load();
        endX = (int) (getX() + 1);
        endY = (int) (getY() + 1);
    }

    @Override
    public void move() {
        switch (dir) {
            case ConstGame.Direction.RIGHT:
                x += getSpeed();
                break;
            case ConstGame.Direction.LEFT:
                x -= getSpeed();
                break;
            case ConstGame.Direction.UP:
                y -= getSpeed();
                break;
            case ConstGame.Direction.DOWN:
                y += getSpeed();
                break;
            default:
                break;
        }
    }


    @Override
    public void render(ScreenGame screenGame, int now) {
        if (alive) {
            if (moving) {
                if (dir == ConstGame.Direction.RIGHT || dir == ConstGame.Direction.UP) {
                    screenGame.drawSprite(Sprite.movingSprite(FRAME[4],
                            FRAME[5], FRAME[6], now), getX(), getY());
                } else {
                    screenGame.drawSprite(Sprite.movingSprite(FRAME[1],
                            FRAME[2], FRAME[3], now), getX(), getY());
                }
            } else {

                if (dir == ConstGame.Direction.RIGHT || dir == ConstGame.Direction.UP) {
                    screenGame.drawSprite(FRAME[4], getX(), getY());
                } else {
                    screenGame.drawSprite(FRAME[1], getX(), getY());
                }

            }
        } else {
            if (old != now) {
                old = now;
                t++;
            }
            screenGame.drawSprite(Sprite.movingSprite(FRAME[7], FRAME[8],
                    FRAME[9], FRAME[10], t), getX(), getY());
        }
        screenGame.drawMiniMap(getX(), getY(), ConstGame.MiniMapColor.ENEMY);
    }

    @Override
    public void kill() {
        Random random = new Random();
        int t = random.nextInt(2);
        if(t > 0){
            t = random.nextInt(10);
            if(t == 1){
                money = new Money(x,y,3);
            }else if(t>=2&&t<=4){
                money = new Money(x,y,2);
            } else {
                money = new Money(x,y,1);
            }
        }
        super.kill();
    }

    @Override
    public void update() {

        if (alive && moving) {
            move();

//            switch (getDir()) {
//                case ConstGame.Direction.UP:
//                    break;
//                case ConstGame.Direction.DOWN:
//                    break;
//                case ConstGame.Direction.LEFT:
//                    break;
//                case ConstGame.Direction.RIGHT:
//                    break;
//            }
        }
    }

    public void update(int[][] tem) {
        if (alive) {
            moving = checkMap(tem);
            while (!moving) {
                calculateMove();
            }
        } else {
            moving = false;
        }
    }

    public void update(int[][] tem, Player player) {
        if (alive) {
            moving = checkMap(tem);

            while (!moving) {
                dir = ai.calculateMove(getX(), getY(), player.getX(), player.getY());
                moving = true;
//                if (Math.abs(endX - getX()) == 1 || Math.abs(endY - getY()) == 1) {
//                    endX = (int) getX();
//                    endY = (int) getY();
//                    update();
//                }

            }

        } else {
            moving = false;
        }

    }

    @Override
    public void calculateMove() {
        dir = ai.calculateMove();
        moving = true;
    }
}
