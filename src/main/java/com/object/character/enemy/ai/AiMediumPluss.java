package com.object.character.enemy.ai;

public class AiMediumPluss extends Ai {
    public int k = 4;
    @Override
    public int calculateMove(float x, float y, float playerX, float playerY) {
        return calculateMove();
    }

    @Override
    public int calculateMove() {
        if (k % 4 == 0) k = 4;
        k--;
        return k % 4 + 1;
    }
}
