package com.object.tile.item;

import com.graphics.ScreenGame;
import com.graphics.Sprite;
import com.hs.ConstGame;
import com.object.character.enemy.Enemy;

import java.util.List;

public class Portal extends Item {

    public Portal(float x, float y, Sprite sprite) {
        super(x, y, sprite);
    }

    @Override
    protected void load() {
    }

    @Override
    public void render(ScreenGame screenGame) {
        screenGame.drawMiniMap(getX(),getY(), ConstGame.MiniMapColor.PORTAL);
        super.render(screenGame);
    }

    public boolean isDone(List<Enemy> enemies) {
        return super.isDone() && enemies.isEmpty();
    }
}
