package com.object.character.enemy;

import com.graphics.Sprite;
import com.object.character.enemy.ai.AiHigh;
import com.object.character.enemy.ai.AiMedium;
import com.object.character.enemy.ai.AiMediumPluss;

public class Oneal extends Enemy {

    public Oneal(float x, float y) {
        super(x, y);
        ai = new AiMedium();
        moving = true;
        score = 200;
    }

    @Override
    protected void load() {
        FRAME[1] = Sprite.oneal_left1;
        FRAME[2] = Sprite.oneal_left2;
        FRAME[3] = Sprite.oneal_left3;
        FRAME[4] = Sprite.oneal_right1;
        FRAME[5] = Sprite.oneal_right2;
        FRAME[6] = Sprite.oneal_right3;
        FRAME[7] = Sprite.oneal_dead;
        FRAME[8] = Sprite.mob_dead1;
        FRAME[9] = Sprite.mob_dead2;
        FRAME[10] = Sprite.mob_dead3;
    }

}
