package com.map;

import com.graphics.Render;
import com.graphics.ScreenGame;
import com.hs.ConstGame;
import com.object.bom.Bom;
import com.object.character.player.Bomber;
import javafx.scene.shape.Rectangle;

/**
 * class kiểm soát thành phần dc vẽ lên màn hình.
 * <p>tạo độ sử dụng là pixel.</p>
 */
public class MapView implements Render {

    private Rectangle view;

    private Layer layerMap;

    private final MiniMap miniMap;


    public MapView(double w, double h,double w1,double h1){
        view = new Rectangle(0,0,w,h);
        layerMap = new Layer();
        miniMap = new MiniMap(w1,h1);
    }

    public void load(int level, Bomber player){
        layerMap.setMap(LoadMap.load(level));
        layerMap.load(player);
        miniMap.setSize(layerMap.getMap().x,layerMap.getMap().y);
    }


    /**
     * cập nhật tạo độ cua mapView trên map gốc.
     * @param bomber nhân vật của người chơi.
     */
    public void update(Bomber bomber) {
        view.setX(Coordinates.tileToPixel( bomber.getXOffset()));
        view.setY(Coordinates.tileToPixel( bomber.getYOffset()));
        layerMap.update();
        layerMap.getMap().update(bomber);
    }

    @Override
    public void render(ScreenGame screenGame) {
        screenGame.drawStaticImage(layerMap.getMap().staticMap,
                view.getX(), view.getY(),view.getWidth(),view.getHeight(),
                0,0,view.getWidth(), view.getHeight());

        screenGame.drawStaticImage(layerMap.getBrickImage(),
                view.getX(), view.getY(),view.getWidth(),view.getHeight(),
                0,0,view.getWidth(), view.getHeight());
    }

    @Override
    public void render(ScreenGame screenGame, int now) {
        render(screenGame);
        layerMap.render(screenGame,now);
    }

    /**
     * trả về Width.
     * @return đơn vị tọa độ.
     */
    public float getWidth(){
        return (float) Coordinates.pixelToTile1(view.getWidth());
    }

    /**
     * trả về Height.
     * @return đơn vị tọa độ.
     */
    public float getHeight(){
        return (float) Coordinates.pixelToTile1(view.getHeight());
    }

    public void setLayerMap(Layer layerMap) {
        this.layerMap = layerMap;
    }

    public MapLevel getMap() {
        return layerMap.getMap();
    }

    public float getGocX(){
        return (float) Coordinates.pixelToTile1(view.getX());
    }
    public float getGocXReal(){
        return (float) view.getX() / ConstGame.InfoObject.SCALED_SIZE;
    }

    public float getGocY(){
        return (float) Coordinates.pixelToTile1(view.getY());
    }

    public float getGocYReal(){
        return (float) view.getY() / ConstGame.InfoObject.SCALED_SIZE;
    }
    public MiniMap getMiniMap() {
        return miniMap;
    }

    @Override
    public void update() {
    }
    public void reset(){
        miniMap.reset();
        layerMap.reset();
        view = null;
    }
}
