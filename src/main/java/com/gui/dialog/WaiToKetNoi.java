package com.gui.dialog;

import com.socket.Online;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;

import java.util.Optional;

public class WaiToKetNoi {
    public static void show(Online online){
        Dialog<Boolean> dialog = new Dialog<>();

        dialog.setTitle("Đang kết nối");

        dialog.setHeaderText("Đang kết nối");

        ButtonType ok = new ButtonType("ok", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(ok, ButtonType.CANCEL);

        dialog.setResultConverter(btn->{
            online.connect();
            return true;
        });

        Optional<Boolean> res = dialog.showAndWait();


    }
}
