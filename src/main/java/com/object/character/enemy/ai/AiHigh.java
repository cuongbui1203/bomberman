package com.object.character.enemy.ai;

import com.hs.ConstGame;

public class AiHigh extends Ai {
    public AiHigh() {
    }

    @Override
    public int calculateMove(float x, float y, float playerX, float playerY) {
        if (true) {
            //int v = calculateRowDirection();
            if (x - playerX > 0.1)
                return ConstGame.Direction.LEFT;
            else if (playerX - x > 0.1)
                return ConstGame.Direction.RIGHT;
            // return calculateColDirection();
            if (y - playerY > 0.1)
                return ConstGame.Direction.UP;
            else if (playerY - y > 0.1)
                return ConstGame.Direction.DOWN;

        } else {
            //int h = calculateColDirection();
            if (playerY < y)
                return ConstGame.Direction.UP;
            else if (playerY > y)
                return ConstGame.Direction.DOWN;

            if (playerX < x)
                return ConstGame.Direction.LEFT;
            else if (playerX > x)
                return ConstGame.Direction.RIGHT;
        }
        return 0;
    }

    @Override
    public int calculateMove() {
        return 0;
    }


}
