package com.hs;

import javafx.scene.paint.Color;

/**
 * các hằng số của game.
 */
public class ConstGame {
    /**
     * hằng số liên quan đến các thông số cơ bản của game
     * như tên,dường đẫn đến các file.
     */
    public static class InfoGame {
        public static final String NAME = "BomberMan";

        public static final int SIZE_TEXTURE = 256;

        public static final String PATH_FXML = "Game.fxml";

        public static final String PATH_DATABASE = "jdbc:sqlite:res/database/database.db";

        public static final String PATH_TEXTURE = "/com/graphics/classic.png";
        public static final String PATH_TEXTURE_SKIN = "/com/graphics/skin.png";
        public static final String PATH_LOAD_MAP = "src/main/resources/levels/Level%d.txt";
        public static final String PATH_HD = "src/main/resources/hd.txt";

        public static final String BGM_PATH = "res/sound/bgm.mp3";
        public static final String EX_PATH = "res/sound/exMus.mp3";
        public static final String LEVEL_UP_PATH = "res/sound/levelUp.mp3";

        public static final String ICON_PATH = "Bomberman.png";
        public static final String ICON_2_PATH = "setting.png";
        public static final int MAX_LEVEL = 2;

        private static final int width = 15;
        public static final int WIDTH = InfoObject.SCALED_SIZE * width;

        private static final int height = 10;
        public static final int HEIGHT = InfoObject.SCALED_SIZE * height;

    }

    /**
     * hằng số liên quan đến thông tin của các vật thể trong game.
     */
    public static class InfoObject {
        public static final int DEFAULT_SIZE = 16;
        public static final int SCALED = 3;
        public static final int SCALED_SIZE = DEFAULT_SIZE * SCALED;

        // color
        public static final int TRANSPARENT_COLOR = customColor(255, 0, 255);
        public static final int SELECTED_PLAYER_COLOR_RED = customColor(255, 0, 0);
        public static final int SELECTED_PLAYER_COLOR_BLUE = customColor(0, 0, 255);
        public static final int SELECTED_PLAYER_COLOR_YELLOW = customColor(255, 255, 0);
        public static final int SELECTED_PLAYER_COLOR_GREEN = customColor(0, 255, 0);
        public static final int PLAYER_COLOR_GOC = 0xff3cbcff;

        public static final int BOM_EXPLOSION_TIME = 3; // second.
        public static final int WALL_PASS_TIME = 4;

        public static final float SPEED_EXCHANGE_DENTAL = 0.0005F;

        public static final float SPEED_ENEMY = 0.02F; // m/s.

        public static final float SPEED_CHARACTER = 0.035F; // m/s.

        public static int customColor(int r, int g, int b) {
            String sr = Integer.toHexString(r);
            String sg = Integer.toHexString(g);
            String sb = Integer.toHexString(b);
            sr = sr.length() < 2 ? "0" + sr : sr;
            sb = sb.length() < 2 ? "0" + sb : sb;
            sg = sg.length() < 2 ? "0" + sg : sg;
            String stringRes = "ff" + sr + sg + sb;
            return (int) Long.parseLong(stringRes, 16);
        }

    }

    public static class TT {
        public static final int SEVER = 1;
        public static final int CLIENT = 2;
    }

    public static class Direction {
        public static final int UP = 1;
        public static final int RIGHT = 2;
        public static final int DOWN = 3;
        public static final int LEFT = 4;
    }

    public static class MiniMapColor {
        public static final Color PLAYER = Color.rgb(0, 0, 255);
        public static final Color ENEMY = Color.rgb(255, 0, 0);
        public static final Color WALL = Color.rgb(128, 128, 128);
        public static final Color GRASS = Color.rgb(0, 255, 0);
        public static final Color PORTAL = Color.rgb(255, 255, 0);
        public static final Color TELEPORT = Color.rgb(128, 255, 255);
    }
}
