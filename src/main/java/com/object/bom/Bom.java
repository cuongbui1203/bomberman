package com.object.bom;

import com.graphics.ScreenGame;
import com.graphics.Sprite;
import com.hs.ConstGame;
import com.map.MapLevel;
import com.object.Base;
import com.object.bom.flame.FlameSegment;
import com.object.character.Character;
import com.object.character.enemy.Enemy;
import com.object.character.player.Bomber;
import javafx.scene.effect.Light;

import java.util.*;

public class Bom extends Base {

    private final FlameSegment flameSegment;
    private final List<Light.Point> points;
    private final List<Integer> attackedEnemy;
    private int lengthFlame = 1;
    private Timer timer;
    private Timer timer1;
    private int countdown;
    private int explosionFrame = 3;
    private int bombFrame = 0;

    private int up = 0;
    private int right = 0;
    private int down = 0;
    private int left = 0;

    private boolean check = true;

    public Bom(float x, float y, int[][] temp, int flameSegmentLengthBonus) {
        super(x, y, null);

        attackedEnemy = new ArrayList<>();

        points = new ArrayList<>();

        points.add(new Light.Point());//up
        points.add(new Light.Point());//right
        points.add(new Light.Point());//down
        points.add(new Light.Point());//left

        lengthFlame += flameSegmentLengthBonus;

        flameSegment = new FlameSegment(x, y);
        temp[(int) y+1][(int) x+1] = 2;
        countdown = ConstGame.InfoObject.BOM_EXPLOSION_TIME;

        checkMap(temp);

        start();

    }

    private void start() {
        timer = new Timer();
        timer1 = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                update1();
            }
        }, 0, 1000);
        timer1.schedule(new TimerTask() {
            @Override
            public void run() {
                bombFrame++;
            }
        }, 0, 400);
    }


    public void update1() {
        countdown--;
        if (countdown == 0) {
            explosion();
        }
    }

    private void explosionEven() {
        if (timer == null)
            setTimer(new Timer(), null);
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                update2();
            }
        }, 0, 400);
    }

    private void update2() {
        explosionFrame--;
        if (explosionFrame == 0) {
            timer.cancel();
        }
    }

    public boolean endCountdown() {
        boolean res = (countdown <= 0) && check;
        if (res) {
            countdown = -1;
        }
        return res;
    }

    public void setEndCountDownEnd(){
        check = false;
    }

    public void explosion() {
        beforeEx();
        countdown = -1;
        explosionEven();
    }

    public boolean end() {
        return explosionFrame == 0;
    }

    /**
     * Xử lý bom nổ nhiều quả trong tầm.
     *
     * @param boms list bomb.
     * @param temp mảng vật cản.
     */
    public void ex(List<Bom> boms, int[][] temp, MapLevel mapLevel) {
        Iterator<Bom> iterator = boms.iterator();
        while (iterator.hasNext()) {
            Bom a = iterator.next();
            if (a != this) {
                if (countdown <= 0 && checkAnoBomb(a)) {

                    flameSegment.addCenterPoint(a.x, a.y);
                    flameSegment.update(a.up, a.right, a.down, a.left);

                    a.beforeEx();
                    mapLevel.destroy(a);
                    temp[(int) a.y + 1][(int) a.x + 1] = 0;
                    iterator.remove();
                }
            }
        }
    }

    private boolean checkAnoBomb(Bom a) {
        if (getX() == a.getX()) {
            if (getY() > a.getY()) {
                return Math.abs(a.getY() - getY()) <= up;
            } else {
                return Math.abs(a.getY() - getY()) <= down;
            }
        } else if (getY() == a.getY()) {
            if(getX() > a.getX()){
                return Math.abs(getX() - a.getX()) <= left;
            }else {
                return Math.abs(getX() - a.getX()) <= right;
            }
        }else {
            return false;
        }
    }

    @Override
    public void update() {
    }

    @Override
    public void render(ScreenGame screenGame, int now) {

        if (countdown > 0)
            screenGame.drawSprite(Sprite.movingSprite(Sprite.bomb, Sprite.bomb_1, Sprite.bomb_2, Sprite.bomb_1, bombFrame)
                    , getX(), getY());
        else {
            flameSegment.render(screenGame, explosionFrame + 2);
        }
    }

    public void remove(List<Bom> boms, MapLevel mapLevel) {
        mapLevel.temp[(int) y + 1][(int) x + 1] = 0;
        mapLevel.remove2();
        boms.remove(this);
    }

    public void checkMap(int[][] temp) {
        int x = (int) getX() + 1;
        int y = (int) getY() + 1;

        boolean upcheck = false;
        boolean leftcheck = false;
        boolean rightcheck = false;
        boolean downcheck = false;

        for (int i = 1; i <= lengthFlame && !rightcheck; i++) {
            if (temp[y][x + i] == 0) {
                right++;
            } else {
                rightcheck = true;
            }
        }

        for (int i = 1; i <= lengthFlame && !upcheck; i++) {
            if (temp[y - i][x] == 0) {
                up++;
            } else {
                upcheck = true;
            }
        }

        for (int i = 1; i <= lengthFlame && !downcheck; i++) {
            if (temp[y + i][x] == 0) {
                down++;
            } else {
                downcheck = true;
            }
        }

        for (int i = 1; i <= lengthFlame && !leftcheck; i++) {
            if (temp[y][x - i] == 0) {
                left++;
            } else {
                leftcheck = true;
            }
        }


        flameSegment.update(up, right, down, left);

        if (up < lengthFlame) {
            setTd(0, getX(), getY() - up - 1);
        } else {
            setTd(0, getX(), getY() - up);
        }

        if (right < lengthFlame) {
            setTd(1, getX() + right + 1, getY());
        } else {
            setTd(1, getX() + right, getY());
        }

        if (down < lengthFlame) {
            setTd(2, getX(), getY() + down + 1);
        } else {
            setTd(2, getX(), getY() + down);
        }

        if (left < lengthFlame) {
            setTd(3, getX() - left - 1, getY());
        } else {
            setTd(3, getX() - left, getY());
        }

    }

    private void setTd(int pos, float x, float y) {
        points.get(pos).setX(x);
        points.get(pos).setY(y);
    }

    private void setTimer(Timer timer, Timer timer1) {
        this.timer = timer;
        this.timer1 = timer1;
    }

    private void beforeEx() {
        if (timer != null)
            timer.cancel();
        if (timer1 != null)
            timer1.cancel();
        timer = null;
        timer1 = null;
    }

    public List<Light.Point> getPoints() {
        return points;
    }

    public void attack(Character player, List<Enemy> enemies, int score){
        flameSegment.attack(player,score);
        for(int i = 0; i< enemies.size(); i++){
            if(!attackedEnemy.contains(i)) {
                flameSegment.attack(enemies.get(i), score);
                attackedEnemy.add(i);
            }
        }
    }

    public void attack(List<Bomber> players, List<Enemy> enemies, int score) {
        for(Bomber bomber : players)
            flameSegment.attack(bomber,score);
        for(int i = 0; i< enemies.size(); i++){
            if(!attackedEnemy.contains(i)) {
                flameSegment.attack(enemies.get(i), score);
                attackedEnemy.add(i);
            }
        }
    }
}
