package com.object.character.enemy;

import com.graphics.Sprite;
import com.object.character.enemy.ai.AiLow;

public class Ming extends Enemy{

    public Ming(float x, float y) {
        super(x, y);
        ai = new AiLow();
        score = 1000;

    }

    @Override
    protected void load() {
        FRAME[1] = Sprite.ming_left1;
        FRAME[2] = Sprite.ming_left2;
        FRAME[3] = Sprite.ming_left3;
        FRAME[4] = Sprite.ming_right1;
        FRAME[5] = Sprite.ming_right2;
        FRAME[6] = Sprite.ming_right3;
        FRAME[7] = Sprite.ming_dead;
        FRAME[8] = Sprite.mob_dead1;
        FRAME[9] = Sprite.mob_dead2;
        FRAME[10] = Sprite.mob_dead3;
    }

}
