package com.map;

import com.graphics.ImageMerger;
import com.graphics.Render;
import com.graphics.ScreenGame;
import com.graphics.Sprite;
import com.hs.ConstGame;
import com.object.Base;
import com.object.bom.Bom;
import com.object.character.enemy.*;
import com.object.character.player.Bomber;
import com.object.tile.*;
import com.object.tile.item.*;
import javafx.scene.effect.Light;
import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 * class quản lý Map.
 */
public class MapLevel implements Render {

    public int x;
    public int y;
    public int level;

    public char[][] mapChar;
    public Base[][] obj;
    public Sprite[][] map;
    public int[][] temp;

    public Image staticMap;

    public List<Teleport> teleports;

    public List<Enemy> enemies;
    public List<Brick> bricks;
    public List<Brick> brick2s;
    public List<Item> items;

    public Bomber player;

    private int numOfBrick = 0;


    /**
     * <p>r <=> y (dọc)</p>
     * <p>c <=> x (ngang)</p>
     */
    public MapLevel(int level, int r, int c) {
        this.x = c;
        this.y = r;
        this.level = level;
        init();
    }

    public void init() {
        // mảng các object
        obj = new Base[y][x];

        // mảng hình ảnh.
        map = new Sprite[y][x];

        //mảng ký tự đọc từ map.
        mapChar = new char[y][x];

        // mảng lưu vị trí và kiểm soát các Item,Brick,Enemy,Portal,Teleport.
        items = new ArrayList<>();
        bricks = new ArrayList<>();
        enemies = new ArrayList<>();
        brick2s = new ArrayList<>();
        teleports = new ArrayList<>();

        // temp là mảng có vật cản
        temp = new int[y + 2][x + 2];

    }

    @Override
    public void update() {
        bricks.removeIf(Brick::isBeforeDestroy);
    }

    public void update(Bomber player) {
        update();
        for (Item a : items) {
            if (!(a instanceof Portal))
                a.update(player);
        }
    }

    @Override
    public void render(ScreenGame screenGame) {
        for (Brick a : brick2s) {
            if (a.isBeforeDestroy())
                a.render(screenGame);
        }
    }

    @Override
    public void render(ScreenGame screenGame, int now) {
        drawMiniMap(screenGame);
        for (Item a : items) {
            if (a.isShow())
                a.render(screenGame);
        }
        for (Brick a : brick2s) {
            if (a.isBeforeDestroy())
                a.render(screenGame, now);
        }
        for (Teleport t : teleports) {
            t.render(screenGame);
        }
    }

    public void drawMiniMap(ScreenGame screenGame) {
        for (int i = 0; i < map.length; i++) {
            for (int t = 0; t < map[i].length; t++) {
                if (map[i][t] == Sprite.wall) {
                    screenGame.drawMiniMap(t, i, ConstGame.MiniMapColor.WALL);
                } else {
                    screenGame.drawMiniMap(t, i, ConstGame.MiniMapColor.GRASS);
                }
            }
        }
        for (Brick a : brick2s) {
            screenGame.drawMiniMap(a.getX(), a.getY(), ConstGame.MiniMapColor.WALL);
        }
    }

    /**
     * hàm load các obj dựa trên map char dc đọc từ file txt.
     * temp:
     * + 0: đi đc
     * + 1: tường and gạch.
     * + 2: bom.
     * + 3: Door.
     */
    public void loadObj() {
        int id = 0;
        for (int i = 0; i < y + 2; i++) {
            for (int t = 0; t < x + 2; t++) {
                temp[i][t] = 0;
            }
        }

        for (int i = 0; i < y; i++) {
            for (int t = 0; t < x; t++) {
                switch (mapChar[i][t]) {
                    // Tile
                    case '#':
                        obj[i][t] = new Wall(t, i, Sprite.wall);
                        temp[i + 1][t + 1] = 1;
                        break;
                    case '*': {
                        int xx = Math.abs(new Random().nextInt(100) + 1);
                        if (xx < 90) {
                            obj[i][t] = new Brick(t, i, Sprite.brick);
                            bricks.add((Brick) obj[i][t]);
                            brick2s.add((Brick) obj[i][t]);
                        } else {
                            obj[i][t] = new BrickPlus(t, i, Sprite.brick_plus);
                            bricks.add((BrickPlus) obj[i][t]);
                            brick2s.add((BrickPlus) obj[i][t]);
                        }
                        temp[i + 1][t + 1] = 1;
                    }
                    break;
                    case '|':
                        obj[i][t] = new BrickPlus(t, i, Sprite.brick_plus);
                        bricks.add((BrickPlus) obj[i][t]);
                        brick2s.add((BrickPlus) obj[i][t]);
                        temp[i + 1][t + 1] = 1;
                        break;
                    // Teleport
                    case 'a':
                        obj[i][t] = new Door(t, i);
                        teleports.add(new Teleport((Door) obj[i][t], 'a'));
                        temp[i + 1][t + 1] = 3;
                        break;
                    case 'A':
                        obj[i][t] = new Door(t, i);
                        temp[i + 1][t + 1] = 3;
                        for (Teleport e : teleports) {
                            if (e.getId() == 'a') {
                                e.addDoor2((Door) obj[i][t]);
                            }
                        }
                        break;
                    case 'c':
                        obj[i][t] = new Door(t, i);
                        teleports.add(new Teleport((Door) obj[i][t], 'c'));
                        temp[i + 1][t + 1] = 3;
                        break;
                    case 'C':
                        obj[i][t] = new Door(t, i);
                        temp[i + 1][t + 1] = 3;
                        for (Teleport e : teleports) {
                            if (e.getId() == 'c') {
                                e.addDoor2((Door) obj[i][t]);
                            }
                        }
                        break;
                    case 'd':
                        obj[i][t] = new Door(t, i);
                        teleports.add(new Teleport((Door) obj[i][t], 'd'));
                        temp[i + 1][t + 1] = 3;
                        break;
                    case 'D':
                        obj[i][t] = new Door(t, i);
                        temp[i + 1][t + 1] = 3;
                        for (Teleport e : teleports) {
                            if (e.getId() == 'd') {
                                e.addDoor2((Door) obj[i][t]);
                            }
                        }
                        break;

                    // Player
                    case 'p':
                        obj[i][t] = new Grass(t, i, Sprite.grass);
                        player.setX(t);
                        player.setY(i);
                        break;

                    // Enemy
                    case '0':
                        obj[i][t] = new Minvo(t, i);
                        enemies.add((Enemy) obj[i][t]);
                        break;
                    case '1':
                        obj[i][t] = new Balloon(t, i);
                        enemies.add((Enemy) obj[i][t]);
                        break;
                    case '2':
                        obj[i][t] = new Oneal(t, i);
                        enemies.add((Enemy) obj[i][t]);
                        break;
                    case '3':
                        obj[i][t] = new Doll(t, i);
                        enemies.add((Enemy) obj[i][t]);
                        break;
                    case '4':
                        obj[i][t] = new Kondoria(t, i);
                        enemies.add((Enemy) obj[i][t]);
                        break;
                    case '5':
                        obj[i][t] = new Angry(t, i);
                        enemies.add((Enemy) obj[i][t]);
                        break;
                    case '6':
                        obj[i][t] = new Cuon(t, i);
                        enemies.add((Enemy) obj[i][t]);
                        break;
                    case '7':
                        obj[i][t] = new Cuonh(t, i);
                        enemies.add((Enemy) obj[i][t]);
                        break;
                    case '8':
                        obj[i][t] = new Ming(t, i);
                        enemies.add((Enemy) obj[i][t]);
                        break;
                    case '9':
                        obj[i][t] = new Mun(t, i);
                        enemies.add((Enemy) obj[i][t]);
                        break;

                    // Item
                    case 'b': {
                        obj[i][t] = new BombItem(t, i, Sprite.powerup_bombpass);
                        Brick brick = new Brick(t, i, Sprite.brick, id);
                        bricks.add(brick);
                        brick2s.add(brick);
                        id++;
                        temp[i + 1][t + 1] = 1;
                        items.add((Item) obj[i][t]);
                        break;
                    }
                    case 'f': {
                        obj[i][t] = new FlameItem(t, i, Sprite.powerup_flames);
                        Brick brick2 = new Brick(t, i, Sprite.brick, id);
                        bricks.add(brick2);
                        brick2s.add(brick2);
                        id++;
                        temp[i + 1][t + 1] = 1;
                        items.add((Item) obj[i][t]);
                        break;
                    }
                    case 's': {
                        obj[i][t] = new SpeedItem(t, i, Sprite.powerup_speed);
                        Brick brick1 = new Brick(t, i, Sprite.brick, id);
                        bricks.add(brick1);
                        brick2s.add(brick1);
                        id++;
                        temp[i + 1][t + 1] = 1;
                        items.add((Item) obj[i][t]);
                        break;
                    }
                    case 'w': {
                        obj[i][t] = new WallPass(t, i, Sprite.powerup_wallpass);
                        Brick brick1 = new Brick(t, i, Sprite.brick, id);
                        bricks.add(brick1);
                        brick2s.add(brick1);
                        id++;
                        temp[i + 1][t + 1] = 1;
                        items.add((Item) obj[i][t]);
                        break;
                    }

                    // Portal
                    case 'x':
                        obj[i][t] = new Portal(t, i, Sprite.portal);
                        Brick brick3 = new Brick(t, i, Sprite.brick, id);
                        bricks.add(brick3);
                        brick2s.add(brick3);
                        id++;
                        temp[i + 1][t + 1] = 1;
                        items.add((Portal) obj[i][t]);
                        break;

                    default:
                        obj[i][t] = new Grass(t, i, Sprite.grass);
                        break;
                }

                if (obj[i][t] instanceof Wall || obj[i][t] instanceof Teleport) {
                    map[i][t] = Sprite.wall;
                } else {
                    map[i][t] = Sprite.grass;
                }

            }
        }
        staticMap = ImageMerger.createStaticImage(map);
        numOfBrick = bricks.size();
    }

    /**
     * kiểm tra xem có tường nào bị phá k để render lại layer tường.
     *
     * @return true nếu có. false nếu k.
     */
    public boolean remove() {
        boolean res = numOfBrick != bricks.size();
        numOfBrick = bricks.size();
        return res;
    }

    public void remove2() {
        Iterator<Brick> b = brick2s.iterator();
        while (b.hasNext()) {
            Brick t = b.next();
            if (t.isDestroyed()) {
                temp[(int) t.getY() + 1][(int) t.getX() + 1] = 0;
                if (t.getId() != -1) {
                    items.get(t.getId()).Show();
                }
                b.remove();
            }
        }
    }

    /**
     * xóa bricks khi đặt bom dựa vào các tọa độ bị tác động.
     *
     * @param bom quả bom được đặt xuống.
     */
    public void remove(Bom bom) {
        for (int i = 0; i < bricks.size(); i++) {
            for (Light.Point p : bom.getPoints()) {
                if (bricks.get(i).getX() == p.getX() && bricks.get(i).getY() == p.getY()) {
                    bricks.get(i).setBeforeDestroy();
//                    System.out.println(bricks.get(i));
                }
            }
        }
    }

    /**
     * Phá hủy tường khi có bom nổ.
     *
     * @param bom quả bom nổ.
     */
    public void destroy(Bom bom) {
        for (Brick brick2 : brick2s) {
            if (!bom.getPoints().isEmpty()) {
                Iterator<Light.Point> ite = bom.getPoints().iterator();
                while (ite.hasNext()) {
                    Light.Point p = ite.next();
                    if (brick2.getX() == p.getX() && brick2.getY() == p.getY() && brick2.isBeforeDestroy()) {
                        brick2.destroy();
                        ite.remove();
                    }
                }
            }
        }
    }

    public void reset() {
        for (Teleport t : teleports) {
            t.end();
        }
        teleports = null;
        mapChar = null;
        obj = null;
        map = null;
        temp = null;
        staticMap = null;
        enemies = null;
        bricks = null;
        brick2s = null;
        items = null;
    }
}
