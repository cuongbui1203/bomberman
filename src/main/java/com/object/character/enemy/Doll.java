package com.object.character.enemy;

import com.graphics.Sprite;
import com.object.character.enemy.ai.AiLow;

public class Doll extends Enemy{

    public Doll(float x, float y) {
        super(x, y);
        ai = new AiLow();
        score = 500;

    }

    @Override
    public double getSpeed() {
        return super.getSpeed()*1.2;
    }

    @Override
    protected void load() {
        FRAME[1] = Sprite.doll_left1;
        FRAME[2] = Sprite.doll_left2;
        FRAME[3] = Sprite.doll_left3;
        FRAME[4] = Sprite.doll_right1;
        FRAME[5] = Sprite.doll_right2;
        FRAME[6] = Sprite.doll_right3;
        FRAME[7] = Sprite.doll_dead;
        FRAME[8] = Sprite.mob_dead1;
        FRAME[9] = Sprite.mob_dead2;
        FRAME[10] = Sprite.mob_dead3;
    }

}
