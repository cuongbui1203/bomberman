package com.graphics;

import com.hs.ConstGame;
import com.map.MapView;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

/**
 * class quản lý việc vẽ lên màn hình của game.
 */
public class ScreenGame {
    private final GraphicsContext gc;
    private final GraphicsContext miniMapGc;
    private MapView map;
    private float miniMapSize;

    public void reset(){
        map = null;
    }

    public ScreenGame(Canvas mainGame, Canvas miniMap) {
        gc = mainGame.getGraphicsContext2D();
        miniMapGc = miniMap.getGraphicsContext2D();
    }

    public void setMap(MapView map) {
        this.map = map;
        miniMapSize = map.getMiniMap().getSize();
    }

    public void drawSprite(Sprite sprite, double x, double y) {
        if (x + 1 >= map.getGocX() && x <= map.getGocX() + map.getWidth()
                && y + 1 >= map.getGocY() && y <= map.getHeight() + map.getGocY()) {
            gc.drawImage(sprite.getFxImage(),
                    calculateX((float) x) * ConstGame.InfoObject.SCALED_SIZE,
                    calculateY((float) y) * ConstGame.InfoObject.SCALED_SIZE);
        }
    }

    public void drawStaticImage(Image image, double sx, double sy, double sw, double sh,
                                double dx, double dy, double dw, double dh) {
        gc.drawImage(image, sx, sy, sw, sh, dx, dy, dw, dh);
    }

    /**
     * hàm xóa màn hình.
     */
    public void clearScreen() {
        gc.clearRect(0, 0, gc.getCanvas().getWidth(), gc.getCanvas().getHeight());
        miniMapGc.clearRect(0, 0,
                map.getMiniMap().getWidth(), map.getMiniMap().getHeight());
    }

    public void drawString(String text){
        gc.setFont(new Font(150));
        gc.setTextAlign(TextAlignment.CENTER);
        gc.fillText(text,gc.getCanvas().getWidth()/2,gc.getCanvas().getHeight()/2);
    }

    public void drawMiniMap(float x, float y, Color color) {
        miniMapGc.setFill(color);
        miniMapGc.fillRect(calculateMiniMapX(x * miniMapSize)
                , calculateMiniMapY(y * miniMapSize), miniMapSize, miniMapSize);
    }

    public void drawMiniMapView() {
        miniMapGc.setStroke(Color.rgb(255,255,140));
        for(int i = 0;i<= 2;i++) {
            miniMapGc.strokeRect(calculateMiniMapX(map.getGocXReal() * miniMapSize + i)
                    , calculateMiniMapY(map.getGocYReal() * miniMapSize + i)
                    , map.getWidth() * miniMapSize - 2*i
                    , map.getHeight() * miniMapSize - 2*i);
        }
    }

    /**
     * tính toán tọa độ x.
     *
     * @param x tọa độ x của vật thể trên map gốc.
     * @return tạo độ x của vật thể trên mapView.
     */
    private float calculateX(float x) {
        return x - map.getGocX();
    }

    /**
     * tính toán tọa độ y.
     *
     * @param y tọa độ y của vật thể trên map gốc.
     * @return tạo độ y của vật thể trên mapView.
     */
    private float calculateY(float y) {
        return y - map.getGocY();
    }

    /**
     * tính toán tọa độ x.
     *
     * @param x tọa độ x của vật thể trên map gốc.
     * @return tạo độ x của vật thể trên miniMap.
     */
    private float calculateMiniMapX(float x) {
        return x + map.getMiniMap().getGocX();
    }

    /**
     * tính toán tọa độ y.
     *
     * @param y tọa độ y của vật thể trên map gốc.
     * @return tạo độ y của vật thể trên miniMap.
     */
    private float calculateMiniMapY(float y) {
        return y + map.getMiniMap().getGocY();
    }
}
