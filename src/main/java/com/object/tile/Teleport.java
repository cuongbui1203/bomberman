package com.object.tile;

import com.graphics.ScreenGame;
import com.object.character.player.Player;

import java.util.Timer;
import java.util.TimerTask;

public class Teleport extends Tile {
    // cổng 1.
    private final Door door1;
    // id.
    private final char id;
    // cổng 2.
    private Door door2;
    private boolean ss;
    private int timeCountdown;
    private final Timer timer = new Timer();

    public Teleport(Door door1, char id) {
        super(-1, -1, null);
        this.door1 = door1;
        this.id = id;
        ss = true;
        timeCountdown = 2;
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (!ss) {
                    timeCountdown--;
                }
                if (timeCountdown == 0) {
                    ss = true;
                    timeCountdown = 2;
                }
            }
        }, 0, 1000);
    }

    public void check(Player player) {
        if(ss) {
            if (door1.check(player)) {
                player.setTD(door2.getX(), door2.getY());
                ss = false;
            } else if (door2.check(player)) {
                player.setTD(door1.getX(), door1.getY());
                ss = false;
            }
        }
    }

    public void addDoor2(Door door2) {
        this.door2 = door2;
    }

    public Door getDoor1() {
        return door1;
    }

    public char getId() {
        return id;
    }

    public Door getDoor2() {
        return door2;
    }

    @Override
    public void render(ScreenGame screenGame) {
        door1.render(screenGame);
        door2.render(screenGame);
    }

    public void end(){
        timer.cancel();
    }
}
