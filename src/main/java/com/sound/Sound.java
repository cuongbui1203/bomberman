package com.sound;

import com.hs.ConstGame;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

import java.io.File;

public class Sound {

    private final MediaPlayer bgmPlayer;
    private final MediaPlayer soundPlayer;
    private final MediaPlayer sound2Player;

    public Sound() {
        System.out.println(this);
        soundPlayer = new MediaPlayer(new Media(new File(ConstGame.InfoGame.EX_PATH).toURI().toString()));
        sound2Player = new MediaPlayer(new Media(new File(ConstGame.InfoGame.LEVEL_UP_PATH).toURI().toString()));
        bgmPlayer = new MediaPlayer(new Media(new File(ConstGame.InfoGame.BGM_PATH).toURI().toString()));
        soundPlayer.setVolume(0.5);
        bgmPlayer.setVolume(0);

        soundPlayer.onEndOfMediaProperty().setValue(new Runnable() {
            @Override
            public void run() {
                soundPlayer.seek(new Duration(0));
                soundPlayer.pause();
            }
        });

        sound2Player.onEndOfMediaProperty().setValue(new Runnable() {
            @Override
            public void run() {
                sound2Player.seek(new Duration(0));
                sound2Player.pause();
            }
        });
        bgmPlayer.onEndOfMediaProperty().setValue(new Runnable() {
            @Override
            public void run() {
                bgmPlayer.seek(new Duration(0));
            }
        });
        bgmPlayer.volumeProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                if (newValue.doubleValue() == 0) {
                    bgmPlayer.pause();
                } else {
                    bgmPlayer.play();
                }
            }
        });
    }

    public void playBgm() {
        bgmPlayer.setAutoPlay(true);
    }

    public void exSound() {
        soundPlayer.play();
    }

    public void levelUpSound() {
        sound2Player.play();
    }


    public void end() {
        bgmPlayer.stop();
        soundPlayer.stop();
        sound2Player.stop();
    }

    public double getBgmVolume() {
        return bgmPlayer.getVolume();
    }

    public void setBgmVolume(double val) {
        bgmPlayer.setVolume(val);
    }

    public double getSoundVolume() {
        return soundPlayer.getVolume();
    }

    public void setSoundVolume(double val) {
        sound2Player.setVolume(val);
        soundPlayer.setVolume(val);
    }
}
