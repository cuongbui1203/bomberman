package com.object.character.enemy;

import com.graphics.Sprite;
import com.object.character.enemy.ai.AiLow;

public class Cuonh extends Enemy{

    public Cuonh(float x, float y) {
        super(x, y);
        ai = new AiLow();
        score = 1000;

    }

    @Override
    protected void load() {
        FRAME[1] = Sprite.Cuonh_left1;
        FRAME[2] = Sprite.Cuonh_left2;
        FRAME[3] = Sprite.Cuonh_left3;
        FRAME[4] = Sprite.Cuonh_right1;
        FRAME[5] = Sprite.Cuonh_right2;
        FRAME[6] = Sprite.Cuonh_right3;
        FRAME[7] = Sprite.Cuonh_dead;
        FRAME[8] = Sprite.mob_dead1;
        FRAME[9] = Sprite.mob_dead2;
        FRAME[10] = Sprite.mob_dead3;
    }

}
