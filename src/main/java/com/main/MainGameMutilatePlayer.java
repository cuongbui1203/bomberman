package com.main;

import com.database.DatabaseController;
import com.graphics.ScreenGame;
import com.gui.GuiController;
import com.gui.dialog.AlertSetting;
import com.hs.ConstGame;
import com.input.Keyboard2;
import com.map.Coordinates;
import com.map.MapView;
import com.object.bom.Bom;
import com.object.character.enemy.Enemy;
import com.object.character.player.Bomber;
import com.object.tile.Teleport;
import com.object.tile.item.Item;
import com.object.tile.item.Portal;
import com.sound.Sound;
import javafx.animation.AnimationTimer;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MainGameMutilatePlayer {

    private final ScreenGame screenGame;
    private final Keyboard2 in;
    private final Label labelScore;
    private final Canvas mainGame;
    private final Canvas miniMap;
    private final GuiController gui;
    private final Sound sound;
    private final AnimationTimer timer;
    private final List<Bomber> players;
    private MapView mapView;
    private List<Bom> bombs;
    private List<Bom> bombs2;
    private List<Item> items;
    private List<Enemy> enemies;
    private List<Boolean> attacked;
    private int score;
    private boolean running = false;
    private int level;
    private boolean endLevel = false;

    private final Label lifeCount;
    private final Label lifeCount2;
    private final Label levelLabel;

    /**
     * hàm khởi tạo game.
     *
     * @param mainGame     nơi game dc vẽ.
     * @param miniMap      nơi vẽ minimap.
     * @param labelScore   nơi hiển thị điểm.
     * @param lifeCount    đếm số mạng p1
     * @param lifeCount2    đếm số mạng p2
     * @param levelLabel   đếm level.
     * @param input        quản lý bàn phím.
     * @param gui          quản lý giao diện người dùng.
     * @param playerStatus thông tin trạng thái nhân vật.
     * @param sound        quản lý âm thanh.
     */
    public MainGameMutilatePlayer(Canvas mainGame, Canvas miniMap, Label labelScore, Label lifeCount, Label lifeCount2,
                                  Label levelLabel, Keyboard2 input, GuiController gui,
                                  Pair<String, Pair<Integer, Boolean>> playerStatus,
                                  Pair<String, Pair<Integer, Boolean>> playerStatusTwo,
                                  Sound sound) {
        Pair<Integer, Boolean> playerStatus2 = playerStatus.getValue();
        Pair<Integer, Boolean> playerStatus3 = playerStatusTwo.getValue();
        this.lifeCount2 = lifeCount2;
        this.sound = sound;
        this.gui = gui;
        this.mainGame = mainGame;
        this.miniMap = miniMap;
        this.in = input;
        this.labelScore = labelScore;
        this.lifeCount = lifeCount;
        this.levelLabel = levelLabel;
        score = 0;
        level = 1;

        players = new ArrayList<>();
        attacked = new ArrayList<>();
        attacked.add(Boolean.FALSE);
        attacked.add(Boolean.FALSE);
        players.add(new Bomber(0,1, 1, playerStatus2.getKey(), playerStatus2.getValue(), playerStatus.getKey()));
        players.add(new Bomber(1,3, 2, playerStatus3.getKey(), playerStatus3.getValue(), playerStatusTwo.getKey()));
        players.get(0).setInput(this.in);
        players.get(1).setInput(this.in);

        lifeCount.setText(String.valueOf(players.get(0).getHeat()));
        lifeCount2.setText(String.valueOf(players.get(1).getHeat()));

        screenGame = new ScreenGame(mainGame, miniMap);

        init(level);
        // vòng lặp chính của game.
        timer = new AnimationTimer() {
            private long old = 0;
            private long old2 = 0;
            private int f = 0;
            private int time = 0;

            @Override
            public void handle(long now) {

                f++;
                if (running) {
                    render(time);
                    update();
                }

                if (in.exit) {
//                    gui.goHome();
                    stopGame();
                }
                // FPS
                if (now - old >= 1000000000) {

                    gui.fpsShow.setText(String.format("FPS: %4d", f));

                    f = 0;
                    old = now;
                }
                // ki
                if (now - old2 >= 400000000) {
                    time++;
                    if (time == 30) {
                        time = 0;
                    }
                    old2 = now;
                    attacked.set(0,Boolean.FALSE);
                    attacked.set(1,Boolean.FALSE);
                }

            }
        };
        timer.start();

    }
    public void nextLevel() {
        level++;
        endLevel = false;
        if (level <= ConstGame.InfoGame.MAX_LEVEL) {
            reset();
            init(level);
            run();
        } else {
            stop();
            screenGame.clearScreen();
            screenGame.drawString("You Win");
            saveScore();
        }
    }

    /**
     * chơi lại.
     *
     * @param dau true nếu chơi lại từ đầu.
     */
    public void restart(boolean dau) {
        reset();
        level = dau ? 1 : level;
        init(level);
        run();
    }


    public void init(int level) {

        mapView = new MapView(mainGame.getWidth(), mainGame.getHeight(), miniMap.getWidth(), miniMap.getHeight());

        mapView.load(level, players.get(0));

        enemies = mapView.getMap().enemies;
        items = mapView.getMap().items;

        screenGame.setMap(mapView);

        bombs = new ArrayList<>();
        bombs2 = new ArrayList<>();


        players.get(0).setMapH(mapView.getMap().y);
        players.get(0).setMapW(mapView.getMap().x);
        players.get(0).setMapViewH(mapView.getHeight());
        players.get(0).setMapViewW(mapView.getWidth());
        players.get(1).setMapH(mapView.getMap().y);
        players.get(1).setMapW(mapView.getMap().x);
        players.get(1).setMapViewH(mapView.getHeight());
        players.get(1).setMapViewW(mapView.getWidth());

    }

    public void run() {
        running = true;
    }

    public void stop() {
        running = false;
    }

    public void destroy() {
        timer.stop();
    }

    public void reset() {
        players.get(0).reborn();
        players.get(1).reborn();
        bombs = null;
        bombs2 = null;
        enemies = null;
        items = null;
        screenGame.reset();
        mapView.reset();
    }

    /**
     * Hàm Render chính của luồng game.
     *
     * @param now số khung hình hiện tại.
     */
    private void render(int now) {
        //xóa màn hình.
        screenGame.clearScreen();

        // cập nhật điểm lên Label.
        labelScore.setText(String.valueOf(score));
        // cập nhật số mạng.
        lifeCount.setText(String.valueOf(players.get(0).getHeat()));
        lifeCount2.setText(String.valueOf(players.get(1).getHeat()));
        // Cập nhật level hiển thị
        levelLabel.setText(String.valueOf(level));

        if (players.get(0).isDie()) {
            // vẽ khi player chết.
            screenGame.drawString("You Lost");
            saveScore();
        } else {
            // render các thành phần tĩnh.
            mapView.render(screenGame, now);

            // render bom.
            if (!bombs.isEmpty()) {
                for (Bom a : bombs) {
                    a.render(screenGame, now);
                }
            }

            if (!bombs2.isEmpty()) {
                for (Bom a : bombs2) {
                    a.render(screenGame, now);
                }
            }

            // render Enemy.
            for (Enemy e : enemies) {
                e.render(screenGame, now);
            }

            // render Player.
            players.get(0).render(screenGame, now);
            players.get(1).render(screenGame, now);

            // vẽ khung map View lên miniMap.
            screenGame.drawMiniMapView();
        }

        if (players.get(1).isDie()) {
            // vẽ khi player chết.
            screenGame.drawString("You Lost");
            saveScore();
        } else {
            // render Player.
            players.get(1).render(screenGame, now);
        }
    }

    /**
     * Hàm cập nhật chính của game.
     */
    private void update() {
        // Kiểm tra teleport.
        for (Teleport t : mapView.getMap().teleports) {
            t.check(players.get(0));
        }

        // va chạm của player với map.
        players.get(0).checkMap(mapView.getMap().temp);
        players.get(1).checkMap(mapView.getMap().temp);

        // cập nhật player.
        players.get(0).update();
        players.get(1).update();
//        System.out.println(player.getX()+ " " + player.getY());
        // kiểm tra va chạm với
        players.get(0).checkEnemy(enemies);
        players.get(1).checkEnemy(enemies);

        // Kiểm tra 2 Enemy va chạm với nhau
//        for (Enemy e:enemies){
//            e.checkEnemy(enemies);
//        }

        // Cập nhật màn hình hiển thị.
        mapView.update(players.get(0));
//        mapView.update(players.get(1));

        // kiểm tra đặt bom.
        if (in.Attack.get(0) && !attacked.get(0)
                && mapView.getMap().temp[(int) (players.get(0).getY() + 1.5)][(int) (players.get(0).getX() + 1.5)] == 0
                && bombs.size() < players.get(0).getNumBomb()) {
            final Bom t = new Bom((int) (players.get(0).getX()+0.5), (int) (players.get(0).getY()+0.5),
                    mapView.getMap().temp, players.get(0).getFlameSegmentBonus());
            bombs.add(t);
            mapView.getMap().remove(t);
            attacked.set(0,Boolean.TRUE);
        }
        if (in.Attack.get(1) && !attacked.get(1)
                && mapView.getMap().temp[(int) (players.get(1).getY() + 1.5)][(int) (players.get(1).getX() + 1.5)] == 0
                && bombs2.size() < players.get(1).getNumBomb()) {
            final Bom t = new Bom((int) (players.get(1).getX()+0.5), (int) (players.get(1).getY()+0.5),
                    mapView.getMap().temp, players.get(1).getFlameSegmentBonus());
            bombs2.add(t);
            mapView.getMap().remove(t);
            attacked.set(1,Boolean.TRUE);
        }

        // cập nhật cho Enemy cũng như cộng điểm nếu tiêu diệt dc Enemy và xóa khỏi List cho trc.
        Iterator<Enemy> iterator = enemies.iterator();
        while (iterator.hasNext()) {
            Enemy e = iterator.next();
            // Cập nhật chiều chuyển động
            e.update(mapView.getMap().temp, players.get(0));
            //cập nhật tọa độ.
            e.update();
            if (e.isDie()) {
                score += e.getScore();
                iterator.remove();
            }
        }

        // Cập nhật cho các thành phần bị ảnh hưởng khi quả bom hết thời gian countdown.
        for (int i = 0; i < bombs.size(); i++) {
            if (bombs.get(i).endCountdown()) {
                sound.exSound();
                bombs.get(i).ex(bombs, mapView.getMap().temp, mapView.getMap());
                bombs.get(i).attack(players, enemies, score);
                mapView.getMap().destroy(bombs.get(i));
                bombs.get(i).setEndCountDownEnd();
            }
        }
        for (int i = 0; i < bombs2.size(); i++) {
            if (bombs2.get(i).endCountdown()) {
                sound.exSound();
                bombs2.get(i).ex(bombs2, mapView.getMap().temp, mapView.getMap());
                bombs2.get(i).attack(players, enemies, score);
                mapView.getMap().destroy(bombs2.get(i));
                bombs2.get(i).setEndCountDownEnd();
            }
        }

        // Cập nhật để xóa những quả bom đã nổ xong.
        for (Bom a : bombs) {
            if (a.end()) {
                a.remove(bombs, mapView.getMap());
                mapView.getMap().remove2();
//                System.out.println("End Life");
                break;
            }
        }
        for (Bom a : bombs2) {
            if (a.end()) {
                a.remove(bombs2, mapView.getMap());
                mapView.getMap().remove2();
//                System.out.println("End Life");
                break;
            }
        }

        // kiểm tra khi player đi qua Portal (cửa sang màn tiếp theo).
        for (Item e : items) {
            if (e instanceof Portal && Coordinates.check(players.get(0),e) && enemies.isEmpty()) {
                nextLevel();
            }
        }
        if(enemies.isEmpty()&&!endLevel){
            sound.levelUpSound();
            endLevel = true;
        }
    }

    public int getScore() {
        return score;
    }

    private void stopGame() {

        timer.stop();

        in.exit = false;

        AlertSetting.show(sound,gui,timer);
    }

    private void saveScore() {
        timer.stop();
        Alert alert = new Alert(Alert.AlertType.NONE);

        alert.setTitle("lưu Điểm");
        alert.setHeaderText("lưu Điểm");

        String timeFormat = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yy HH:mm"));

        ButtonType saveBtn = new ButtonType("Lưu", ButtonBar.ButtonData.OK_DONE);

        alert.getDialogPane().getButtonTypes().addAll(saveBtn, ButtonType.CANCEL);

        TextField playerName = new TextField();
        playerName.setText(players.get(0).getName());

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        Label Score = new Label(String.valueOf(score));

        grid.add(new Label("Tên:"), 0, 0);
        grid.add(playerName, 1, 0);
        grid.add(new Label("Điểm:"), 0, 1);
        grid.add(Score, 1, 1);

        alert.getDialogPane().setContent(grid);

        alert.setOnHidden(new EventHandler<DialogEvent>() {
            @Override
            public void handle(DialogEvent event) {
                if (alert.getResult() == saveBtn) {
                    DatabaseController.addToDatabase(playerName.getText(), timeFormat, Integer.parseInt(Score.getText()));
                    Alert alert1 = new Alert(Alert.AlertType.INFORMATION);
                    alert1.setTitle("Thành Công");
                    alert1.setHeaderText("Lưu Thành Công");
                    alert1.show();
                }
                gui.goHome();
            }
        });
        alert.show();
    }
}
