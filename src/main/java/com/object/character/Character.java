package com.object.character;

import com.graphics.Sprite;
import com.hs.ConstGame;
import com.map.Coordinates;
import com.object.Base;
import com.object.character.enemy.Enemy;

import java.util.List;

public abstract class Character extends Base {

    protected Sprite[] FRAME = new Sprite[20];
    protected boolean alive;
    protected boolean moving;
    protected int dir = ConstGame.Direction.RIGHT;
    protected double speed;
    protected boolean canMove;
    protected int score;
    protected int old;
    protected int t;
    protected int lastDir = dir;

    public Character(float x, float y) {
        super(x, y, null);
        moving = false;
        alive = true;
        canMove = true;
    }

    protected abstract void load();

    public abstract void calculateMove();

    public abstract void move();

    public int getDir() {
        return dir;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public void kill() {
        if (t == 0)
            alive = false;
    }

    public boolean checkMap(int[][] temp) {
        float x = getX();
        float y = getY();
        if (lastDir != getDir()) {
            if (x - (int) x >= 0.75) {
                x = (int) x + 1;
                this.x = x;
            }

            if (y - (int) y >= 0.75) {
                y = (int) y + 1;
                this.y = y;
            }
        }

        switch (getDir()) {
            case ConstGame.Direction.RIGHT:
                x += 1 + getSpeed();
                break;
            case ConstGame.Direction.LEFT:
                x -= getSpeed();
                break;
            case ConstGame.Direction.UP:
                y -= getSpeed();
                break;
            case ConstGame.Direction.DOWN:
                y += 1 + getSpeed();
                break;
        }
        if (temp[(int) y + 1][(int) x + 1] == 0) {
            lastDir = dir;
            return true;
        }
        return false;
    }

    public void checkEnemy(List<Enemy> enemies) {
        for (Enemy e : enemies) {
            if (Coordinates.check3(this, e) && e != this) {
                reverseDir();
                e.reverseDir();
            }
        }
    }

    public void reverseDir() {
        if (dir == ConstGame.Direction.UP)
            dir = ConstGame.Direction.DOWN;

        if (dir == ConstGame.Direction.DOWN)
            dir = ConstGame.Direction.UP;

        if (dir == ConstGame.Direction.RIGHT)
            dir = ConstGame.Direction.LEFT;

        if (dir == ConstGame.Direction.LEFT)
            dir = ConstGame.Direction.RIGHT;
    }

    public int getScore() {
        return score;
    }

    public boolean isDie() {
        return !alive && t >= 4;
    }
}
