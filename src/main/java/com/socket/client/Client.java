package com.socket.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {
    private String ip = "localhost";
    private int port;
    private Socket socket;
    private DataInputStream dis;
    private DataOutputStream dos;

    public Client(int port) {
        this.port = port;
        try {
            socket = new Socket(ip, port);
            dis = new DataInputStream(socket.getInputStream());
            dos = new DataOutputStream(socket.getOutputStream());

        } catch (IOException e) {
            System.out.println("Error Client constructor");
            e.printStackTrace();
            System.exit(-2);
        }
    }

    public Client(String ip,int port){
        this.port = port;
        try {
            socket = new Socket(ip,port);
            dis = new DataInputStream(socket.getInputStream());
            dos = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public void sendData(String data){
        try {
            dos.writeUTF(data);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String reserveData(){
        String res = "null";
        try {
            res = dis.readUTF();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return res;
    }

    public void close(){
        try {
            socket.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean isClose(){
        return socket.isClosed();
    }
}
