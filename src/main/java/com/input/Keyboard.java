package com.input;

import javafx.scene.input.KeyEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * class xử lý sự kiện bàn phím.
 */
public class Keyboard extends KeyOnline {

    public boolean up = false;
    public boolean down = false;
    public boolean left = false;
    public boolean right = false;
    public boolean attack = false;
    public boolean exit = false;

    public List<Boolean> Up = new ArrayList<>();
    public List<Boolean> Down = new ArrayList<>();
    public List<Boolean> Left = new ArrayList<>();
    public List<Boolean> Right = new ArrayList<>();
    public List<Boolean> Attack = new ArrayList<>();
//    protected List<Boolean> Exit = new ArrayList<>();

    public Keyboard() {
//        main
        Up.add(Boolean.FALSE);
        Down.add(Boolean.FALSE);
        Left.add(Boolean.FALSE);
        Right.add(Boolean.FALSE);
        Attack.add(Boolean.FALSE);
//        another
        Up.add(Boolean.FALSE);
        Down.add(Boolean.FALSE);
        Left.add(Boolean.FALSE);
        Right.add(Boolean.FALSE);
        Attack.add(Boolean.FALSE);
//        Exit.add(Boolean.FALSE);
//        if(KeyOnline.sever)

        System.out.println(this);
    }

    protected void keySend(){
        send = "";
        send += up ? "W" : "N";
        send += down ? "S" : "N";
        send += left ? "A" : "N";
        send += right ? "D" : "N";
        send += attack ? "T" : "N";
        send += "N";
        super.keySend();
    }
    /**
     * hàm gọi khi phím dc bấm.
     *
     * @param e even dc gọi.
     */
    public void keyPressed(KeyEvent e) {
        switch (e.getCode()) {
            case W:
                up = true;
                Up.set(0,Boolean.TRUE);
                break;
            case UP:
                up = true;
                Up.set(1,Boolean.TRUE);
                break;
            case S:
                down = true;
                Down.set(0,Boolean.TRUE);
                break;
            case DOWN:
                down = true;
                Down.set(1,Boolean.TRUE);
                break;
            case A:
                left = true;
                Left.set(0,Boolean.TRUE);
                break;
            case LEFT:
                left = true;
                Left.set(1,Boolean.TRUE);
                break;
            case D:
                right = true;
                Right.set(0,Boolean.TRUE);
                break;
            case RIGHT:
                right = true;
                Right.set(1,Boolean.TRUE);
                break;
            case NUMPAD0:
                attack = true;
                Attack.set(1,Boolean.TRUE);
                break;
            case SPACE:
                attack = true;
                Attack.set(0,Boolean.TRUE);
                break;
            case ESCAPE:
                exit = true;
//                Down.set(0,Boolean.TRUE);
        }
        keySend();
    }

    /**
     * hàm gọi khi phím dc nhả.
     *
     * @param e even dc gọi.
     */
    public void keyReleased(KeyEvent e) {
        switch (e.getCode()) {
            case W:
                up = false;
                Up.set(0,Boolean.FALSE);
                break;
            case UP:
                up = false;
                Up.set(1,Boolean.FALSE);
                break;
            case S:
                down = false;
                Down.set(0,Boolean.FALSE);
                break;
            case DOWN:
                down = false;
                Down.set(1,Boolean.FALSE);
                break;
            case A:
                left = false;
                Left.set(0,Boolean.FALSE);
                break;
            case LEFT:
                left = false;
                Left.set(1,Boolean.FALSE);
                break;
            case D:
                right = false;
                Right.set(0,Boolean.FALSE);
                break;
            case RIGHT:
                right = false;
                Right.set(1,Boolean.FALSE);
                break;
            case NUMPAD0:
                attack = false;
                Attack.set(1,Boolean.FALSE);
                break;
            case SPACE:
                attack = false;
                Attack.set(0,Boolean.FALSE);
                break;
            case ESCAPE:
                exit = false;
//                Down.set(0,Boolean.TRUE);
        }
        keySend();
    }
}
