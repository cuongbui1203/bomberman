package com.object.tile.item;

import com.graphics.Sprite;

public class WallPass extends Item{

    public WallPass(float x, float y, Sprite sprite) {
        super(x, y, sprite);
    }

    @Override
    protected void load() {
        wallPass = true;
    }
}
