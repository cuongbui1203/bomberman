package com.graphics;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

import com.hs.ConstGame;

/**
 * đọc texture vào bộ nhớ.
 */
public class SpriteSheet {

    public static SpriteSheet tiles = new SpriteSheet(ConstGame.InfoGame.PATH_TEXTURE, ConstGame.InfoGame.SIZE_TEXTURE);

    public static SpriteSheet tiles2 = new SpriteSheet(ConstGame.InfoGame.PATH_TEXTURE_SKIN, 384, 384);
    public static SpriteSheet tilesbac = new SpriteSheet("/com/graphics/money2.png", 16);
    public static SpriteSheet tilesvang = new SpriteSheet("/com/graphics/money1.png", 16);
    public static SpriteSheet tileskc = new SpriteSheet("/com/graphics/money3.png", 16);

    public final int SIZE;
    private final String _path;
    public int[] _pixels;
    public BufferedImage image;

    public SpriteSheet(String path, int size) {
        _path = path;
        SIZE = size;
        _pixels = new int[SIZE * SIZE];
        load();
    }

    public SpriteSheet(String path, int sizeW, int sizeH) {
        _path = path;
        SIZE = sizeW / 2;
        _pixels = new int[SIZE * SIZE];
        load2();
    }

    private void load2() {
        try {
            URL a = SpriteSheet.class.getResource(_path);
//            assert a != null;
            BufferedImage tg = ImageIO.read(a);
            // image = ImageIO.read(a);
            int w = tg.getWidth() / 2;
            int h = tg.getHeight() / 2;
            Image image1 = tg.getScaledInstance(w, h, Image.SCALE_DEFAULT);
            image = new BufferedImage(w, h, BufferedImage.TYPE_4BYTE_ABGR);
            image.getGraphics().drawImage(image1, 0, 0, null);
            image.getRGB(0, 0, w, h, _pixels, 0, w);
            // System.out.println(_pixels);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(0);
        }
    }

    public static void main(String[] args) {
//        System.out.println(tilesbac.toString());
        SpriteSheet.class.getResource("/com/graphics/money2.png");
    }

    private void load() {
        try {
            URL a = SpriteSheet.class.getResource(_path);
            image = ImageIO.read(a);
            int w = image.getWidth();
            int h = image.getHeight();
            image.getRGB(0, 0, w, h, _pixels, 0, w);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(0);
        }
    }
}
