package com.object.character.enemy.ai;

import java.util.Random;

public abstract class Ai {
    Random random;
    public Ai(){
        random = new Random();
    }
    public abstract int calculateMove(float x, float y, float playerX, float playerY);
    public abstract int calculateMove();

}
