package com.map;

import com.hs.ConstGame;
import com.object.Base;
import com.object.character.player.Bomber;

public class Coordinates {

    public static int pixelToTile(double i) {
        return (int) (i / ConstGame.InfoObject.SCALED_SIZE);
    }

    public static double pixelToTile1(double i) {
        return (i / ConstGame.InfoObject.SCALED_SIZE);
    }

    public static int tileToPixel(int i) {
        return i * ConstGame.InfoObject.SCALED_SIZE;
    }

    public static int tileToPixel(double i) {
        return (int) (i * ConstGame.InfoObject.SCALED_SIZE);
    }

    // theo minh
    public static boolean check2(Bomber bomber, int[][] temp) {
        // tọa độ main int
        float x = bomber.getX()+1;
        float y = bomber.getY()+1;

        if (temp[(int) y][(int) x] == 2) {
            switch (bomber.getDir()) {
                case ConstGame.Direction.RIGHT:
                    x += 1;
                    break;
                case ConstGame.Direction.LEFT:
                    x -= 1;
                    break;
                case ConstGame.Direction.UP:
                    y -= 1;
                    break;
                case ConstGame.Direction.DOWN:
                    y += 1;
                    break;
            }

            return temp[(int) y][(int) x] != 0 && temp[(int) y][(int) x] != 3;
        }
        float speed = ConstGame.InfoObject.SPEED_CHARACTER;
        switch (bomber.getDir()) {
            case ConstGame.Direction.RIGHT:
                x += 1;
                break;
            case ConstGame.Direction.LEFT:
                x -= speed;
                break;
            case ConstGame.Direction.UP:
                y -= speed;
                break;
            case ConstGame.Direction.DOWN:
                y += 1;
                break;
        }

        return temp[(int) y][(int) x] != 0 && temp[(int) y][(int) x] != 3;
    }

    public static int round(float num) {
        if (num - (int) num <= 0.02) return (int) num + 1;
        else return (int) num;
        //return num - (int) num >= 0.95 ? (int) num + 1 : (int) num;
    }

    /**
     * kiểm tra đè lên nhau.
     *
     * @param a player,quái.
     * @param b item,flame.
     * @return true nếu đè.
     */
    public static boolean check(Base a, Base b) { // Item
        return (a.getX() + 0.5 > b.getX() && b.getX() + 1 > a.getX() && a.getY() + 0.5 > b.getY() && b.getY() + 1 > a.getY());
    }

    public static boolean check3(Base a, Base b) { // enemy vs enemy
        return (a.getX() + 2 > b.getX() && b.getX() + 1 > a.getX() + 1 && a.getY() + 2 > b.getY() && b.getY() + 1 > a.getY() + 1);
    }

    public static boolean check2(Base a, Base b) { // tele
        return (a.getX() + 0.5 > b.getX() && b.getX() + 0.5 > a.getX() && a.getY() + 0.5 > b.getY() && b.getY() + 0.5 > a.getY());
    }

    public static boolean checkE(Base a, Base b) { // enemy
        return (a.getX() + 0.5 > b.getX() - 0.2 && b.getX() + 1 > a.getX() + 0.2
                && a.getY() + 0.5 > b.getY() - 0.2 && b.getY() + 1 > a.getY() + 0.2);    }

    public static boolean check(double x, double y, double w, double h, Base target) {
        return (x + w > target.getX() && target.getX() + 0.6 > x && y + h > target.getY() && target.getY() + 0.9 > y);
    }
}
