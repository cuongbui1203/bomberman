package com.object.tile.item;

import com.graphics.Sprite;
import com.hs.ConstGame;

public class SpeedItem extends Item{

    public SpeedItem(float x, float y, Sprite sprite) {
        super(x, y, sprite);
    }


    @Override
    protected void load() {
        speedExchange = ConstGame.InfoObject.SPEED_EXCHANGE_DENTAL;
    }

}
