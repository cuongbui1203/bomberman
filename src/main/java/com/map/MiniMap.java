package com.map;

import javafx.scene.shape.Rectangle;

public class MiniMap{

    private Rectangle map;
    private int size;


    public MiniMap(double width, double height){
        init(width,height);
    }

    public void init(double w,double h){
        map = new Rectangle(0,0,w,h);
    }

    public float getWidth(){
        return (float) map.getWidth();
    }

    public float getHeight(){
        return (float) map.getHeight();
    }

    public void setX(float val){
        map.setX(val);
    }

    public void setY(float val){
        map.setY(val);
    }

    public void setSize(double width, double height){
        size = (int) (map.getWidth()/width);
        map.setX((map.getWidth() - width*size));
        map.setY((map.getHeight() - size*height));
    }

    public float getGocX(){
        return (float) map.getX();
    }

    public float getGocY(){
        return (float) map.getY();
    }

    public int getSize() {
        return size;
    }

    public void reset(){
        map = null;
        size = -1;
    }
}
