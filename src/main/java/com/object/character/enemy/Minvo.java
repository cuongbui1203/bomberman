package com.object.character.enemy;

import com.graphics.Sprite;
import com.object.character.enemy.ai.AiLow;

public class Minvo extends Enemy{

    public Minvo(float x, float y) {
        super(x, y);
        ai = new AiLow();
        score = 1500;

    }

    @Override
    protected void load() {
        FRAME[1] = Sprite.minvo_left1;
        FRAME[2] = Sprite.minvo_left2;
        FRAME[3] = Sprite.minvo_left3;
        FRAME[4] = Sprite.minvo_right1;
        FRAME[5] = Sprite.minvo_right2;
        FRAME[6] = Sprite.minvo_right3;
        FRAME[7] = Sprite.minvo_dead;
        FRAME[8] = Sprite.mob_dead1;
        FRAME[9] = Sprite.mob_dead2;
        FRAME[10] = Sprite.mob_dead3;
    }

}
