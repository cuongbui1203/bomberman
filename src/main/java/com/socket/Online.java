package com.socket;

import com.hs.ConstGame;
import com.socket.client.Client;
import com.socket.sever.Sever;

public class Online {
    public int tt = -1;

    private Client client;
    private Sever sever;

    private String r = "";

    private Thread thread;
    public void set(int port){
        sever = new Sever(port);
        tt = ConstGame.TT.SEVER;
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                r = sever.reserveData();
            }
        });
        thread.start();
    }


    public void set(String ip,int port){
        try {
            client = new Client(ip,port);
            tt = ConstGame.TT.CLIENT;
            thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    r = client.reserveData();
                }
            });
            thread.start();
        } catch (Exception e) {
            System.out.println("ERROR ket noi Sever game");
            System.exit(-2);
        }
    }

    public void sendData(String data){
        if(tt == ConstGame.TT.CLIENT)
            client.sendData(data);
        else if (tt == ConstGame.TT.SEVER)sever.sendData(data);
    }

    public String reserveData(){
        return r;
    }

    public void close(){
        if(client != null) client.close();
        if(sever != null) sever.close();
    }

    public void connect(){
        if(tt == ConstGame.TT.SEVER)
            sever.connect();
    }
}
