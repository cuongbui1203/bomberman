package com.object.character.enemy;

import com.graphics.ScreenGame;
import com.graphics.Sprite;
import com.hs.ConstGame;
import com.object.character.enemy.ai.AiHigh;

public class Angry extends Enemy {
    private boolean isAngry;

    public Angry(float x, float y) {
        super(x, y);
        ai = new AiHigh();
        score = 10000;
        isAngry = false;
    }

    @Override
    protected void load() {
        FRAME[1] = Sprite.Smlie_left1;
        FRAME[2] = Sprite.Smlie_left2;
        FRAME[3] = Sprite.Smlie_left3;
        FRAME[4] = Sprite.Smlie_right1;
        FRAME[5] = Sprite.Smlie_right2;
        FRAME[6] = Sprite.Smlie_right3;
        FRAME[7] = Sprite.Angry_left1;
        FRAME[8] = Sprite.Angry_left2;
        FRAME[9] = Sprite.Angry_left3;
        FRAME[10] = Sprite.Angry_right1;
        FRAME[11] = Sprite.Angry_right2;
        FRAME[12] = Sprite.Angry_right3;
        FRAME[13] = Sprite.Angry_dead;

        FRAME[14] = Sprite.mob_dead1;
        FRAME[15] = Sprite.mob_dead2;
        FRAME[16] = Sprite.mob_dead3;
    }

    @Override
    public double getSpeed() {
        return isAngry ? super.getSpeed() * 2 : super.getSpeed();
    }

    @Override
    public void render(ScreenGame screenGame, int now) {
        if (alive) {
            if (!isAngry) {
                if (moving) {
                    if (dir == ConstGame.Direction.RIGHT || dir == ConstGame.Direction.UP) {
                        screenGame.drawSprite(Sprite.movingSprite(FRAME[4],
                                FRAME[5], FRAME[6], now), getX(), getY());
                    } else {
                        screenGame.drawSprite(Sprite.movingSprite(FRAME[1],
                                FRAME[2], FRAME[3], now), getX(), getY());
                    }
                } else {

                    if (dir == ConstGame.Direction.RIGHT || dir == ConstGame.Direction.UP) {
                        screenGame.drawSprite(FRAME[4], getX(), getY());
                    } else {
                        screenGame.drawSprite(FRAME[1], getX(), getY());
                    }

                }
            } else {
                if (moving) {
                    if (dir == ConstGame.Direction.RIGHT || dir == ConstGame.Direction.UP) {
                        screenGame.drawSprite(Sprite.movingSprite(FRAME[10],
                                FRAME[11], FRAME[12], now), getX(), getY());
                    } else {
                        screenGame.drawSprite(Sprite.movingSprite(FRAME[7],
                                FRAME[8], FRAME[9], now), getX(), getY());
                    }
                } else {

                    if (dir == ConstGame.Direction.RIGHT || dir == ConstGame.Direction.UP) {
                        screenGame.drawSprite(FRAME[10], getX(), getY());
                    } else {
                        screenGame.drawSprite(FRAME[7], getX(), getY());
                    }

                }
            }
        } else {
            if (old != now) {
                old = now;
                t++;
            }
            screenGame.drawSprite(Sprite.movingSprite(FRAME[13], FRAME[14],
                    FRAME[15], FRAME[16], t), getX(), getY());
        }
        screenGame.drawMiniMap(getX(), getY(), ConstGame.MiniMapColor.ENEMY);
    }

    @Override
    public void kill() {
        if (isAngry) super.kill();
        else isAngry = true;
    }
}
