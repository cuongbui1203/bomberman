package com.main;

import com.database.DatabaseController;
import com.graphics.ScreenGame;
import com.gui.GuiController;
import com.gui.dialog.AlertSetting;
import com.hs.ConstGame;
import com.input.Keyboard;
import com.input.Keyboard2;
import com.map.Coordinates;
import com.map.MapView;
import com.object.bom.Bom;
import com.object.character.enemy.Enemy;
import com.object.character.player.Bomber;
import com.object.tile.Teleport;
import com.object.tile.item.Item;
import com.object.tile.item.Money;
import com.object.tile.item.Portal;
import com.sound.Sound;
import javafx.animation.AnimationTimer;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class MainGame {

    private final ScreenGame screenGame;
    private final Keyboard2 in;
    private final Label labelScore;
    private final Canvas mainGame;
    private final Canvas miniMap;
    private final GuiController gui;
    private final Sound sound;
    private final AnimationTimer timer;
    private final Bomber player;
    private MapView mapView;
    private List<Bom> bombs;
    private List<Money> monies;
    private List<Item> items;
    private List<Enemy> enemies;
    private boolean attacked = false;
    private int score;
    private boolean running = false;
    private int level;
    private boolean endLevel = false;

    private final Label lifeCount;
    private final Label levelLabel;

    /**
     * hàm khởi tạo game.
     *
     * @param mainGame     nơi game dc vẽ.
     * @param miniMap      nơi vẽ minimap.
     * @param labelScore   nơi hiển thị điểm.
     * @param lifeCount    đếm số mạng
     * @param levelLabel   đếm level.
     * @param input        quản lý bàn phím.
     * @param gui          quản lý giao diện người dùng.
     * @param playerStatus thông tin trạng thái nhân vật.
     * @param sound        quản lý âm thanh.
     */
    public MainGame(Canvas mainGame, Canvas miniMap, Label labelScore, Label lifeCount,Label levelLabel, Keyboard2 input, GuiController gui, Pair<String, Pair<Integer, Boolean>> playerStatus, Sound sound) {
        Pair<Integer, Boolean> playerStatus2 = playerStatus.getValue();
        this.sound = sound;
        this.gui = gui;
        this.mainGame = mainGame;
        this.miniMap = miniMap;
        this.in = input;
        this.labelScore = labelScore;
        this.lifeCount = lifeCount;
        this.levelLabel = levelLabel;
        score = 0;
        level = 1;

        player = new Bomber(0,1, 1, playerStatus2.getKey(), playerStatus2.getValue(), playerStatus.getKey());
        player.setInput(this.in);

        lifeCount.setText(String.valueOf(player.getHeat()));

        screenGame = new ScreenGame(mainGame, miniMap);

        init(level);
        // vòng lặp chính của game.
        timer = new AnimationTimer() {
            private long old = 0;
            private long old2 = 0;
            private int f = 0;
            private int time = 0;

            @Override
            public void handle(long now) {

                f++;
                if (running) {
                    render(time);
                    update();
                }

                if (in.exit) {
//                    gui.goHome();
                    stopGame();
                }
                // FPS
                if (now - old >= 1000000000) {

                    gui.fpsShow.setText(String.format("FPS: %4d", f));

                    f = 0;
                    old = now;
                }
                // ki
                if (now - old2 >= 400000000) {
                    time++;
                    if (time == 30) {
                        time = 0;
                    }
                    old2 = now;
                    attacked = false;
                }

            }
        };
        timer.start();

    }

    public void nextLevel() {
        level++;
        endLevel = false;
        if (level <= ConstGame.InfoGame.MAX_LEVEL) {
            reset();
            init(level);
            run();
        } else {
            stop();
            screenGame.clearScreen();
            screenGame.drawString("You Win");
            saveScore();
        }
    }

    /**
     * chơi lại.
     *
     * @param dau true nếu chơi lại từ đầu.
     */
    public void restart(boolean dau) {
        reset();
        level = dau ? 1 : level;
        init(level);
        run();
    }


    public void init(int level) {

        mapView = new MapView(mainGame.getWidth(), mainGame.getHeight(), miniMap.getWidth(), miniMap.getHeight());

        mapView.load(level, player);

        enemies = mapView.getMap().enemies;
        items = mapView.getMap().items;

        screenGame.setMap(mapView);

        bombs = new ArrayList<>();
        monies = new ArrayList<>();


        player.setMapH(mapView.getMap().y);
        player.setMapW(mapView.getMap().x);
        player.setMapViewH(mapView.getHeight());
        player.setMapViewW(mapView.getWidth());

    }

    public void run() {
        running = true;
    }

    public void stop() {
        running = false;
    }

    public void destroy() {
        timer.stop();
    }

    public void reset() {
        player.reborn();
        bombs = null;
        enemies = null;
        items = null;
        screenGame.reset();
        mapView.reset();
    }

    /**
     * Hàm Render chính của luồng game.
     *
     * @param now số khung hình hiện tại.
     */
    private void render(int now) {
        //xóa màn hình.
        screenGame.clearScreen();

        // cập nhật điểm lên Label.
        labelScore.setText(String.valueOf(score));
        // cập nhật số mạng.
        lifeCount.setText(String.valueOf(player.getHeat()));
        // Cập nhật level hiển thị
        levelLabel.setText(String.valueOf(level));

        if (player.isDie()) {
            // vẽ khi player chết.
            screenGame.drawString("You Lost");
            saveScore();
        } else {
            // render các thành phần tĩnh.
            mapView.render(screenGame, now);

            if(!monies.isEmpty()){
                for (Money money: monies){
                    money.render(screenGame);
                }
            }


            // render bom.
            if (!bombs.isEmpty()) {
                for (Bom a : bombs) {
                    a.render(screenGame, now);
                }
            }

            // render Enemy.
            for (Enemy e : enemies) {
                e.render(screenGame, now);
            }

            // render Player.
            player.render(screenGame, now);

            // vẽ khung map View lên miniMap.
            screenGame.drawMiniMapView();
        }
    }

    /**
     * Hàm cập nhật chính của game.
     */
    private void update() {
        // Kiểm tra teleport.
        for (Teleport t : mapView.getMap().teleports) {
            t.check(player);
        }

        // va chạm của player với map.
        player.checkMap(mapView.getMap().temp);

        // cập nhật player.
        player.update();
//        System.out.println(player.getX()+ " " + player.getY());
        // kiểm tra va chạm với
        player.checkEnemy(enemies);

        // Kiểm tra 2 Enemy va chạm với nhau
//        for (Enemy e:enemies){
//            e.checkEnemy(enemies);
//        }

        // Cập nhật màn hình hiển thị.
        mapView.update(player);

        // kiểm tra đặt bom.
        if (in.attack && !attacked
                && mapView.getMap().temp[(int) (player.getY() + 1.5)][(int) (player.getX() + 1.5)] == 0
                && bombs.size() < player.getNumBomb()) {
            final Bom t = new Bom((int) (player.getX()+0.5), (int) (player.getY()+0.5),
                    mapView.getMap().temp, player.getFlameSegmentBonus());
            bombs.add(t);
            mapView.getMap().remove(t);
            attacked = true;
        }

        // cập nhật cho Enemy cũng như cộng điểm nếu tiêu diệt dc Enemy và xóa khỏi List cho trc.
        Iterator<Enemy> iterator = enemies.iterator();
        while (iterator.hasNext()) {
            Enemy e = iterator.next();
            // Cập nhật chiều chuyển động
            e.update(mapView.getMap().temp, player);
            //cập nhật tọa độ.
            e.update();
            if (e.isDie()) {
                score += e.getScore();
                Money tg = e.getMoney();
                if(tg!=null){
                    monies.add(tg);
                }
                iterator.remove();
            }
        }

        ///kiểm tra ăn tiền
        for(int i = 0;i< monies.size();i++){
            if(Coordinates.check(player, monies.get(i))){
                score += monies.get(i).getScore();
                monies.remove(i);
            }
        }

        // Cập nhật cho các thành phần bị ảnh hưởng khi quả bom hết thời gian countdown.
        for (int i = 0; i < bombs.size(); i++) {
            if (bombs.get(i).endCountdown()) {
                sound.exSound();
                bombs.get(i).ex(bombs, mapView.getMap().temp, mapView.getMap());
                bombs.get(i).attack(player, enemies, score);

                mapView.getMap().destroy(bombs.get(i));
                bombs.get(i).setEndCountDownEnd();
            }
        }

        // Cập nhật để xóa những quả bom đã nổ xong.
        for (Bom a : bombs) {
            if (a.end()) {
                a.remove(bombs, mapView.getMap());
                mapView.getMap().remove2();
//                System.out.println("End Life");
                break;
            }
        }

        // kiểm tra khi player đi qua Portal (cửa sang màn tiếp theo).
        for (Item e : items) {
            if (e instanceof Portal && Coordinates.check(player,e) && enemies.isEmpty()) {
                nextLevel();
            }
        }
        if(enemies.isEmpty()&&!endLevel){
            sound.levelUpSound();
            endLevel = true;
        }
    }

    public int getScore() {
        return score;
    }

    private void stopGame() {

        timer.stop();

        in.exit = false;

        AlertSetting.show(sound,gui,timer);
    }

    private void saveScore() {
        timer.stop();
        Alert alert = new Alert(Alert.AlertType.NONE);

        alert.setTitle("lưu Điểm");
        alert.setHeaderText("lưu Điểm");

        String timeFormat = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yy HH:mm"));

        ButtonType saveBtn = new ButtonType("Lưu", ButtonBar.ButtonData.OK_DONE);

        alert.getDialogPane().getButtonTypes().addAll(saveBtn, ButtonType.CANCEL);

        TextField playerName = new TextField();
        playerName.setText(player.getName());

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        Label Score = new Label(String.valueOf(score));

        grid.add(new Label("Tên:"), 0, 0);
        grid.add(playerName, 1, 0);
        grid.add(new Label("Điểm:"), 0, 1);
        grid.add(Score, 1, 1);

        alert.getDialogPane().setContent(grid);

        alert.setOnHidden(new EventHandler<DialogEvent>() {
            @Override
            public void handle(DialogEvent event) {
                if (alert.getResult() == saveBtn) {
                    DatabaseController.addToDatabase(playerName.getText(), timeFormat, Integer.parseInt(Score.getText()));
                    Alert alert1 = new Alert(Alert.AlertType.INFORMATION);
                    alert1.setTitle("Thành Công");
                    alert1.setHeaderText("Lưu Thành Công");
                    alert1.show();
                }
                gui.goHome();
            }
        });
        alert.show();
    }
}