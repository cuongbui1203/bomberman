package com.object.character.enemy;

import com.graphics.ScreenGame;
import com.graphics.Sprite;
import com.hs.ConstGame;
import com.object.character.enemy.ai.AiLow;
import com.object.character.enemy.ai.AiMedium;

public class Mun extends Enemy{

    public Mun(float x, float y) {
        super(x, y);
        ai = new AiMedium();
        score = 1000;

    }

    @Override
    public double getSpeed() {
        return super.getSpeed()*1.2;
    }

    @Override
    protected void load() {
        FRAME[1] = Sprite.Mun_left1;
        FRAME[2] = Sprite.Mun_left2;
        FRAME[3] = Sprite.Mun_left3;
        FRAME[4] = Sprite.Mun_right1;
        FRAME[5] = Sprite.Mun_right2;
        FRAME[6] = Sprite.Mun_right3;
        FRAME[7] = Sprite.Mun_dead;
        FRAME[8] = Sprite.mob_dead1;
        FRAME[9] = Sprite.mob_dead2;
        FRAME[10] = Sprite.mob_dead3;
    }

    @Override
    public void render(ScreenGame screenGame, int now) {
        if (alive) {
            if (moving) {
                if (dir == ConstGame.Direction.RIGHT || dir == ConstGame.Direction.UP) {
                    screenGame.drawSprite(Sprite.movingSprite(FRAME[4],
                            FRAME[5], FRAME[6],FRAME[5], now), getX(), getY());
                } else {
                    screenGame.drawSprite(Sprite.movingSprite(FRAME[1],
                            FRAME[2], FRAME[3],FRAME[2], now), getX(), getY());
                }
            } else {

                if (dir == ConstGame.Direction.RIGHT || dir == ConstGame.Direction.UP) {
                    screenGame.drawSprite(FRAME[4], getX(), getY());
                } else {
                    screenGame.drawSprite(FRAME[1], getX(), getY());
                }

            }
        } else {
            if (old != now) {
                old = now;
                t++;
            }
            screenGame.drawSprite(Sprite.movingSprite(FRAME[7], FRAME[8],
                    FRAME[9], FRAME[10], t), getX(), getY());
        }
        screenGame.drawMiniMap(getX(), getY(), ConstGame.MiniMapColor.ENEMY);

    }
}
