package com.object.tile;

import com.graphics.ScreenGame;
import com.graphics.Sprite;

public class BrickPlus extends Brick {

    protected int count;

    public BrickPlus(float x, float y, Sprite sprite) {
        super(x, y, sprite);
        count = 1;
    }

    public BrickPlus(float x, float y, Sprite sprite, int id) {
        super(x, y, sprite,id);
        count = 1;
    }

    @Override
    public void render(ScreenGame screenGame) {
        if (count != 0) {
            screenGame.drawSprite(Sprite.brick_plus, getX(), getY());
        } else {
            screenGame.drawSprite(Sprite.brick_plus_1, getX(), getY());
        }
    }

    @Override
    public void destroy() {
        if (count != 0) {
            count--;
        } else {
            destroyed = true;
        }
    }
}
