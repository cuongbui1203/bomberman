package com.gui.dialog;

import com.socket.Online;
import com.socket.client.Client;
import com.socket.sever.Sever;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Optional;

public class MultiPlaySetting {

    private static class TextFieldLimited extends TextField {
        private int maxlength;
        public TextFieldLimited() {
            this.maxlength = 10;
        }
        public TextFieldLimited(int maxlength) {
            this.maxlength = maxlength;
        }
        public void setMaxlength(int maxlength) {
            this.maxlength = maxlength;
        }
        @Override
        public void replaceText(int start, int end, String text) {
            // Delete or backspace user input.
            if (text.equals("")) {
                super.replaceText(start, end, text);
            } else if (getText().length() < maxlength) {
                super.replaceText(start, end, text);
            }
        }

        @Override
        public void replaceSelection(String text) {
            // Delete or backspace user input.
            if (text.equals("")) {
                super.replaceSelection(text);
            } else if (getText().length() < maxlength) {
                // Add characters, but don't exceed max length.
                if (text.length() > maxlength - getText().length()) {
                    text = text.substring(0, maxlength- getText().length());
                }
                super.replaceSelection(text);
            }
        }
    }


    public static boolean show(Online online){
        Dialog<Boolean>dialog =  new Dialog<>();
//        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();

        dialog.setTitle("Cài đặt chơi mạng");
        dialog.setHeaderText("Cấu hình chế độ");

        ButtonType checkok = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(checkok,ButtonType.CANCEL);


        VBox vBox = new VBox(2);
        HBox hBox = new HBox(4);
        HBox hBox1 = new HBox(2);
        Label ipLabel = new Label("IP");
        Label portLabel = new Label("PORT");
        TextField ipText = new TextField();
        String ipHost;
        try {
            ipHost = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
        ipText.setText(ipHost);
        TextFieldLimited portText = new TextFieldLimited(5);

        hBox.getChildren().addAll(ipLabel,ipText,portLabel,portText);


        Label labelConnect = new Label("cho");

        Button btnSever = new Button();
        btnSever.setText("Sever");
        btnSever.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                online.set(Integer.parseInt(portText.getText()));
//                Thread thread = new Thread(new Runnable() {
//                    @Override
//                    public void run() {
                        labelConnect.setText("Dang Ket noi ....");
                        online.connect();
//                    }
//                });
//                thread.start();
                labelConnect.setText("Da ket noi thanh cong");
            }
        });

        Button btnClient = new Button("Client");
        btnClient.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                labelConnect.setText("Dang Ket noi ....");
                online.set(ipText.getText(), Integer.parseInt(portText.getText()));
                labelConnect.setText("Da ket noi thanh cong");
            }
        });

        hBox1.getChildren().addAll(btnSever,btnClient);

        vBox.getChildren().addAll(hBox,labelConnect,hBox1);

        dialog.getDialogPane().setContent(vBox);

        dialog.setResultConverter( btn -> ipText.getText().equals(ipHost));

        Optional<Boolean> res = dialog.showAndWait();
        return res.get();
    }

}
