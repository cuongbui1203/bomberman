package com.gui.dialog;

import com.graphics.Sprite;
import com.hs.ConstGame;
import javafx.animation.AnimationTimer;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.util.Optional;

public class DialogChaneSettingPlayer {

    public static Pair<String, Pair<Integer, Boolean>> show() {

        Dialog<Pair<String, Pair<Integer, Boolean>>> dialog = new Dialog<>();

        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(Sprite.icon_1);

        dialog.setTitle("Cài đặt cho nhân vật");
        dialog.setHeaderText("Chọn trang phuc và tên");

        TabPane tabPane = new TabPane();
        Tab tab = new Tab("Co ban");
        Tab tab1 = new Tab("Trang phuc");

        ButtonType checkOk = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(checkOk, ButtonType.CANCEL);

        GridPane gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(10, 20, 10, 10));

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(10, 20, 10, 10));

        ToggleGroup colorGroup = new ToggleGroup();
        ToggleGroup sexGroup = new ToggleGroup();
        ToggleGroup skinGroup = new ToggleGroup();

        RadioButton skin1 = new RadioButton("Skin 1");
        skin1.setToggleGroup(skinGroup);
        skin1.setSelected(true);
        skin1.setId("0");

        RadioButton skin2 = new RadioButton("Skin 2");
        skin2.setToggleGroup(skinGroup);
        skin2.setId("2");

        RadioButton skin3 = new RadioButton("Skin 3");
        skin3.setToggleGroup(skinGroup);
        skin3.setId("3");

        RadioButton skin4 = new RadioButton("Skin 4");
        skin4.setToggleGroup(skinGroup);
        skin4.setId("4");

        RadioButton skin5 = new RadioButton("Skin 5");
        skin5.setToggleGroup(skinGroup);
        skin5.setId("5");

        RadioButton skin6 = new RadioButton("Skin 6");
        skin6.setToggleGroup(skinGroup);
        skin6.setId("6");

        RadioButton skin7 = new RadioButton("Skin 7");
        skin7.setToggleGroup(skinGroup);
        skin7.setId("7");

        RadioButton skin8 = new RadioButton("Skin 8");
        skin8.setToggleGroup(skinGroup);
        skin8.setId("1");

        gridPane.add(skin1, 0, 0);
        gridPane.add(skin2, 0, 1);
        gridPane.add(skin3, 0, 2);
        gridPane.add(skin4, 0, 3);
        gridPane.add(skin5, 1, 0);
        gridPane.add(skin6, 1, 1);
        gridPane.add(skin7, 1, 2);
        gridPane.add(skin8, 1, 3);

        RadioButton male = new RadioButton("Nam");
        male.setToggleGroup(sexGroup);
        male.setSelected(true);
        male.setId("m");

        RadioButton female = new RadioButton("Nữ");
        female.setToggleGroup(sexGroup);
        female.setId("l");

        RadioButton red = new RadioButton("đỏ");
        red.setToggleGroup(colorGroup);
        red.setId("r");

        RadioButton blue = new RadioButton("lam");
        blue.setToggleGroup(colorGroup);
        blue.setSelected(true);
        blue.setId("b");

        RadioButton yellow = new RadioButton("vàng");
        yellow.setToggleGroup(colorGroup);
        yellow.setId("y");

        RadioButton green = new RadioButton("lục");
        green.setToggleGroup(colorGroup);
        green.setId("g");

        RadioButton cus = new RadioButton("Tự chọn");
        cus.setToggleGroup(colorGroup);
        cus.setId("c");

        Label redValue = new Label("");
        redValue.setPrefWidth(25);

        Slider redSlider = new Slider();
        redSlider.setMin(0);
        redSlider.setMax(255);
        // redSlider.setShowTickLabels(true);
        redSlider.setShowTickMarks(true);
        redSlider.setPrefWidth(250);
        redSlider.setValue(255);
        redValue.setText(String.valueOf((int) (redSlider.getValue())));
        redSlider.valueProperty()
                .addListener((observable, oldValue, newValue) -> redValue.setText(String.valueOf(newValue.intValue())));

        Label blueValue = new Label("0");
        blueValue.setPrefWidth(25);

        Slider blueSlider = new Slider();
        blueSlider.setMin(0);
        blueSlider.setMax(255);
        // blueSlider.setShowTickLabels(true);
        blueSlider.setShowTickMarks(true);
        blueSlider.setPrefWidth(250);
        blueSlider.valueProperty().addListener(
                (observable, oldValue, newValue) -> blueValue.setText(String.valueOf(newValue.intValue())));

        Label greenValue = new Label("1");
        greenValue.setPrefWidth(25);

        Slider greenSlider = new Slider();
        greenSlider.setMin(1);
        greenSlider.setMax(255);
        // greenSlider.setShowTickLabels(true);
        greenSlider.setShowTickMarks(true);
        greenSlider.setPrefWidth(250);
        greenSlider.valueProperty().addListener(
                (observable, oldValue, newValue) -> greenValue.setText(String.valueOf(newValue.intValue())));

        Canvas canvas = new Canvas(64, 64);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.drawImage(
                Sprite.player_down(ConstGame.InfoObject.SELECTED_PLAYER_COLOR_RED).getFxImage((int) canvas.getWidth()),
                0, 0);

        // skinGroup.getSelectedToggle()

        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {

                Sprite pl;

                gc.setFill(Color.rgb(255, 0, 255));
                gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());

                if (tab.isSelected()) {
                    if (cus.isSelected()) {
                        redSlider.setDisable(false);
                        redValue.setDisable(false);
                        blueSlider.setDisable(false);
                        blueValue.setDisable(false);
                        greenSlider.setDisable(false);
                        greenValue.setDisable(false);
                    } else {

                        redSlider.setDisable(true);
                        redValue.setDisable(true);
                        blueSlider.setDisable(true);
                        blueValue.setDisable(true);
                        greenSlider.setDisable(true);
                        greenValue.setDisable(true);
                    }
                    int clo = 0;

                    switch (((RadioButton) colorGroup.getSelectedToggle()).getId()) {
                        case "r":
                            clo = ConstGame.InfoObject.SELECTED_PLAYER_COLOR_RED;
                            break;
                        case "b":
                            clo = ConstGame.InfoObject.SELECTED_PLAYER_COLOR_BLUE;
                            break;
                        case "y":
                            clo = ConstGame.InfoObject.SELECTED_PLAYER_COLOR_YELLOW;
                            break;
                        case "g":
                            clo = ConstGame.InfoObject.SELECTED_PLAYER_COLOR_GREEN;
                            break;
                        case "c":
                            clo = ConstGame.InfoObject.customColor(
                                    Integer.parseInt(redValue.getText()),
                                    Integer.parseInt(greenValue.getText()),
                                    Integer.parseInt(blueValue.getText()));
                            break;
                    }

                    pl = ((RadioButton) sexGroup.getSelectedToggle()).getId().equals("m") ? Sprite.player_down(clo)
                            : Sprite.player_nu_down(clo);
                    gc.drawImage(pl.getFxImage((int) canvas.getWidth()), 0, 0);
                }
                if (tab1.isSelected()) {
                    pl = Sprite
                            .player_down_skin(Integer.parseInt(((RadioButton) skinGroup.getSelectedToggle()).getId()));
                    gc.drawImage(pl.getFxImage((int) canvas.getWidth()), 0, 0);
                }
            }
        };

        timer.start();

        Node checkOkBtn = dialog.getDialogPane().lookupButton(checkOk);
        checkOkBtn.setDisable(false);

        TextField name = new TextField();
        name.setText("Player");
        name.textProperty().addListener((observable, oldValue, newValue) -> {
            checkOkBtn.setDisable(name.getText().trim().isEmpty());
        });

        HBox hBox = new HBox();
        hBox.getChildren().add(new Label("Tên nhân vât:"));
        hBox.getChildren().add(name);
        hBox.setPadding(new Insets(0, 20, 0, 20));
        hBox.setSpacing(10);
        // hBox.setAlignment(Pos.CENTER);
        hBox.getChildren().add(canvas);

        VBox vBox = new VBox();
        vBox.getChildren().add(hBox);

        // grid.add(new Label("Tên nhân vât:"), 0, 0);
        // grid.add(name, 1, 0);
        // grid.add(canvas, 3, 1, 2, 2);
        grid.add(male, 0, 1);
        grid.add(female, 0, 2);
        grid.add(red, 1, 1);
        grid.add(blue, 1, 2);
        grid.add(yellow, 1, 3);
        grid.add(green, 1, 4);
        grid.add(cus, 1, 5);
        grid.add(redSlider, 1, 6);
        grid.add(redValue, 2, 6);
        grid.add(greenSlider, 1, 7);
        grid.add(greenValue, 2, 7);
        grid.add(blueSlider, 1, 8);
        grid.add(blueValue, 2, 8);

        tab.setClosable(false);
        tab.setContent(grid);

        tab1.setClosable(false);
        tab1.setContent(gridPane);

        tabPane.getTabs().addAll(tab, tab1);

        vBox.getChildren().add(tabPane);

        dialog.getDialogPane().setContent(vBox);

        dialog.setResultConverter(btn -> {
            timer.stop();
            String nameRes = name.getText().trim();
            RadioButton sexBtn = (RadioButton) sexGroup.getSelectedToggle();

            if (btn == checkOk) {
                if (tab.isSelected()) {
                    RadioButton button = (RadioButton) colorGroup.getSelectedToggle();

                    // System.out.println("color: " + button.getId());
                    // System.out.println("Sex: " + sexBtn.getId());

                    switch (button.getId()) {
                        case "r":
                            return new Pair<>(nameRes, new Pair<>(ConstGame.InfoObject.SELECTED_PLAYER_COLOR_RED,
                                    sexBtn.getId().equals("m")));
                        case "b":
                            return new Pair<>(nameRes, new Pair<>(ConstGame.InfoObject.SELECTED_PLAYER_COLOR_BLUE,
                                    sexBtn.getId().equals("m")));
                        case "y":
                            return new Pair<>(nameRes, new Pair<>(ConstGame.InfoObject.SELECTED_PLAYER_COLOR_YELLOW,
                                    sexBtn.getId().equals("m")));
                        case "g":
                            return new Pair<>(nameRes, new Pair<>(ConstGame.InfoObject.SELECTED_PLAYER_COLOR_GREEN,
                                    sexBtn.getId().equals("m")));
                        case "c":
                            int clo = ConstGame.InfoObject.customColor(Integer.parseInt(redValue.getText()),
                                    Integer.parseInt(greenValue.getText()), Integer.parseInt(blueValue.getText()));
                            return new Pair<>(nameRes, new Pair<>(clo, sexBtn.getId().equals("m")));
                    }
                }
                if (tab1.isSelected()) {
                    int clo = Integer.parseInt(((RadioButton) skinGroup.getSelectedToggle()).getId());
                    return new Pair<>(nameRes, new Pair<>(clo, null));
                }
            }

            return new Pair<>("", null);
        });

        Optional<Pair<String, Pair<Integer, Boolean>>> res1 = dialog.showAndWait();

        Pair<String, Pair<Integer, Boolean>> res = res1.get();

        if (res.getValue() == null) {
            return null;
        }
        return res;

    }

    public static Pair<Pair<String, Pair<Integer, Boolean>>, Pair<String, Pair<Integer, Boolean>>> show2Play() {
        Dialog<Pair<Pair<String, Pair<Integer, Boolean>>, Pair<String, Pair<Integer, Boolean>>>> dialog = new Dialog<>();

        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(Sprite.icon_1);
        TextField name_ = new TextField();

        dialog.setTitle("Cài đặt cho nhân vật");
        dialog.setHeaderText("Chọn trang phuc và tên");

        TabPane tabPane = new TabPane();
        Tab tab = new Tab("Co ban");
        Tab tab1 = new Tab("Trang phuc");

        ButtonType checkOk = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(checkOk, ButtonType.CANCEL);

        GridPane gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(10, 20, 10, 10));

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(10, 20, 10, 10));

        ToggleGroup colorGroup = new ToggleGroup();
        ToggleGroup sexGroup = new ToggleGroup();
        ToggleGroup skinGroup = new ToggleGroup();

        RadioButton skin1 = new RadioButton("Skin 1");
        skin1.setToggleGroup(skinGroup);
        skin1.setSelected(true);
        skin1.setId("0");

        RadioButton skin2 = new RadioButton("Skin 2");
        skin2.setToggleGroup(skinGroup);
        skin2.setId("2");
        skin2.setDisable(true);

        RadioButton skin3 = new RadioButton("Skin 3");
        skin3.setToggleGroup(skinGroup);
        skin3.setId("3");

        RadioButton skin4 = new RadioButton("Skin 4");
        skin4.setToggleGroup(skinGroup);
        skin4.setId("4");

        RadioButton skin5 = new RadioButton("Skin 5");
        skin5.setToggleGroup(skinGroup);
        skin5.setId("5");

        RadioButton skin6 = new RadioButton("Skin 6");
        skin6.setToggleGroup(skinGroup);
        skin6.setId("6");

        RadioButton skin7 = new RadioButton("Skin 7");
        skin7.setToggleGroup(skinGroup);
        skin7.setId("7");

        RadioButton skin8 = new RadioButton("Skin 8");
        skin8.setToggleGroup(skinGroup);
        skin8.setId("1");

        gridPane.add(skin1, 0, 0);
        gridPane.add(skin2, 0, 1);
        gridPane.add(skin3, 0, 2);
        gridPane.add(skin4, 0, 3);
        gridPane.add(skin5, 1, 0);
        gridPane.add(skin6, 1, 1);
        gridPane.add(skin7, 1, 2);
        gridPane.add(skin8, 1, 3);

        RadioButton male = new RadioButton("Nam");
        male.setToggleGroup(sexGroup);
        male.setSelected(true);
        male.setId("m");

        RadioButton female = new RadioButton("Nữ");
        female.setToggleGroup(sexGroup);
        female.setId("l");

        RadioButton red = new RadioButton("đỏ");
        red.setToggleGroup(colorGroup);
        red.setId("r");

        RadioButton blue = new RadioButton("lam");
        blue.setToggleGroup(colorGroup);
        blue.setSelected(true);
        blue.setId("b");

        RadioButton yellow = new RadioButton("vàng");
        yellow.setToggleGroup(colorGroup);
        yellow.setId("y");

        RadioButton green = new RadioButton("lục");
        green.setToggleGroup(colorGroup);
        green.setId("g");

        RadioButton cus = new RadioButton("Tự chọn");
        cus.setToggleGroup(colorGroup);
        cus.setId("c");

        Label redValue = new Label("");
        redValue.setPrefWidth(25);

        Slider redSlider = new Slider();
        redSlider.setMin(0);
        redSlider.setMax(255);
        // redSlider.setShowTickLabels(true);
        redSlider.setShowTickMarks(true);
        redSlider.setPrefWidth(250);
        redSlider.setValue(255);
        redValue.setText(String.valueOf((int) (redSlider.getValue())));
        redSlider.valueProperty()
                .addListener((observable, oldValue, newValue) -> redValue.setText(String.valueOf(newValue.intValue())));

        Label blueValue = new Label("0");
        blueValue.setPrefWidth(25);

        Slider blueSlider = new Slider();
        blueSlider.setMin(0);
        blueSlider.setMax(255);
        // blueSlider.setShowTickLabels(true);
        blueSlider.setShowTickMarks(true);
        blueSlider.setPrefWidth(250);
        blueSlider.valueProperty().addListener(
                (observable, oldValue, newValue) -> blueValue.setText(String.valueOf(newValue.intValue())));

        Label greenValue = new Label("1");
        greenValue.setPrefWidth(25);

        Slider greenSlider = new Slider();
        greenSlider.setMin(1);
        greenSlider.setMax(255);
        // greenSlider.setShowTickLabels(true);
        greenSlider.setShowTickMarks(true);
        greenSlider.setPrefWidth(250);
        greenSlider.valueProperty().addListener(
                (observable, oldValue, newValue) -> greenValue.setText(String.valueOf(newValue.intValue())));

        Canvas canvas = new Canvas(64, 64);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.drawImage(
                Sprite.player_down(ConstGame.InfoObject.SELECTED_PLAYER_COLOR_RED).getFxImage((int) canvas.getWidth()),
                0, 0);

        Node checkOkBtn = dialog.getDialogPane().lookupButton(checkOk);
        checkOkBtn.setDisable(false);

        TextField name = new TextField();
        name.setText("Player");
        name.textProperty().addListener((observable, oldValue, newValue) -> {
            checkOkBtn.setDisable(name.getText().trim().isEmpty() || name_.getText().trim().isEmpty());
        });

        HBox hBox = new HBox();
        hBox.getChildren().add(new Label("Tên nhân vât:"));
        hBox.getChildren().add(name);
        hBox.setPadding(new Insets(0, 20, 0, 20));
        hBox.setSpacing(10);
        // hBox.setAlignment(Pos.CENTER);
        hBox.getChildren().add(canvas);

        VBox vBox = new VBox();
        vBox.getChildren().add(hBox);

        grid.add(male, 0, 1);
        grid.add(female, 0, 2);
        grid.add(red, 1, 1);
        grid.add(blue, 1, 2);
        grid.add(yellow, 1, 3);
        grid.add(green, 1, 4);
        grid.add(cus, 1, 5);
        grid.add(redSlider, 1, 6);
        grid.add(redValue, 2, 6);
        grid.add(greenSlider, 1, 7);
        grid.add(greenValue, 2, 7);
        grid.add(blueSlider, 1, 8);
        grid.add(blueValue, 2, 8);

        tab.setClosable(false);
        tab.setContent(grid);

        tab1.setClosable(false);
        tab1.setContent(gridPane);

        tabPane.getTabs().addAll(tab, tab1);

        vBox.getChildren().add(tabPane);
        HBox hBox1 = new HBox();
        hBox1.getChildren().add(vBox);
        // ------------------------------------------------
        TabPane tabPane2 = new TabPane();
        Tab tab_ = new Tab("Co ban");
        Tab tab1_ = new Tab("Trang phuc");

        GridPane gridPane_ = new GridPane();
        gridPane_.setHgap(10);
        gridPane_.setVgap(10);
        gridPane_.setPadding(new Insets(10, 20, 10, 10));

        GridPane grid_ = new GridPane();
        grid_.setHgap(10);
        grid_.setVgap(10);
        grid_.setPadding(new Insets(10, 20, 10, 10));

        ToggleGroup colorGroup_ = new ToggleGroup();
        ToggleGroup sexGroup_ = new ToggleGroup();
        ToggleGroup skinGroup_ = new ToggleGroup();

        RadioButton skin1_ = new RadioButton("Skin 1");
        skin1_.setToggleGroup(skinGroup_);
        skin1_.setDisable(true);
        skin1_.setId("0");

        RadioButton skin2_ = new RadioButton("Skin 2");
        skin2_.setToggleGroup(skinGroup_);
        skin2_.setId("2");
        skin2_.setSelected(true);

        RadioButton skin3_ = new RadioButton("Skin 3");
        skin3_.setToggleGroup(skinGroup_);
        skin3_.setId("3");

        RadioButton skin4_ = new RadioButton("Skin 4");
        skin4_.setToggleGroup(skinGroup_);
        skin4_.setId("4");

        RadioButton skin5_ = new RadioButton("Skin 5");
        skin5_.setToggleGroup(skinGroup_);
        skin5_.setId("5");

        RadioButton skin6_ = new RadioButton("Skin 6");
        skin6_.setToggleGroup(skinGroup_);
        skin6_.setId("6");

        RadioButton skin7_ = new RadioButton("Skin 7");
        skin7_.setToggleGroup(skinGroup_);
        skin7_.setId("7");

        RadioButton skin8_ = new RadioButton("Skin 8");
        skin8_.setToggleGroup(skinGroup_);
        skin8_.setId("1");

        gridPane_.add(skin1_, 0, 0);
        gridPane_.add(skin2_, 0, 1);
        gridPane_.add(skin3_, 0, 2);
        gridPane_.add(skin4_, 0, 3);
        gridPane_.add(skin5_, 1, 0);
        gridPane_.add(skin6_, 1, 1);
        gridPane_.add(skin7_, 1, 2);
        gridPane_.add(skin8_, 1, 3);

        RadioButton male_ = new RadioButton("Nam");
        male_.setToggleGroup(sexGroup_);
        male_.setSelected(true);
        male_.setId("m");

        RadioButton female_ = new RadioButton("Nữ");
        female_.setToggleGroup(sexGroup_);
        female_.setId("l");

        RadioButton red_ = new RadioButton("đỏ");
        red_.setToggleGroup(colorGroup_);
        red_.setId("r");

        RadioButton blue_ = new RadioButton("lam");
        blue_.setToggleGroup(colorGroup_);
        blue_.setSelected(true);
        blue_.setId("b");

        RadioButton yellow_ = new RadioButton("vàng");
        yellow_.setToggleGroup(colorGroup_);
        yellow_.setId("y");

        RadioButton green_ = new RadioButton("lục");
        green_.setToggleGroup(colorGroup_);
        green_.setId("g");

        RadioButton cus_ = new RadioButton("Tự chọn");
        cus_.setToggleGroup(colorGroup_);
        cus_.setId("c");

        Label redValue_ = new Label("255");
        redValue_.setPrefWidth(25);

        Slider redSlider_ = new Slider();
        redSlider_.setMin(0);
        redSlider_.setMax(255);
        // redSlider.setShowTickLabels(true);
        redSlider_.setShowTickMarks(true);
        redSlider_.setPrefWidth(250);
        redSlider_.setValue(255);
        redValue.setText(String.valueOf((int) (redSlider_.getValue())));
        redSlider_.valueProperty().addListener(
                (observable, oldValue, newValue) -> redValue_.setText(String.valueOf(newValue.intValue())));

        Label blueValue_ = new Label("0");
        blueValue_.setPrefWidth(25);

        Slider blueSlider_ = new Slider();
        blueSlider_.setMin(0);
        blueSlider_.setMax(255);
        // blueSlider.setShowTickLabels(true);
        blueSlider_.setShowTickMarks(true);
        blueSlider_.setPrefWidth(250);
        blueSlider_.valueProperty().addListener(
                (observable, oldValue, newValue) -> blueValue_.setText(String.valueOf(newValue.intValue())));

        Label greenValue_ = new Label("1");
        greenValue_.setPrefWidth(25);

        Slider greenSlider_ = new Slider();
        greenSlider_.setMin(1);
        greenSlider_.setMax(255);
        // greenSlider.setShowTickLabels(true);
        greenSlider_.setShowTickMarks(true);
        greenSlider_.setPrefWidth(250);
        greenSlider_.valueProperty().addListener(
                (observable, oldValue, newValue) -> greenValue_.setText(String.valueOf(newValue.intValue())));

        Canvas canvas_ = new Canvas(64, 64);
        GraphicsContext gc_ = canvas_.getGraphicsContext2D();
        gc_.drawImage(
                Sprite.player_down(ConstGame.InfoObject.SELECTED_PLAYER_COLOR_RED).getFxImage((int) canvas_.getWidth()),
                0, 0);

        grid_.add(male_, 0, 1);
        grid_.add(female_, 0, 2);
        grid_.add(red_, 1, 1);
        grid_.add(blue_, 1, 2);
        grid_.add(yellow_, 1, 3);
        grid_.add(green_, 1, 4);
        grid_.add(cus_, 1, 5);
        grid_.add(redSlider_, 1, 6);
        grid_.add(redValue_, 2, 6);
        grid_.add(greenSlider_, 1, 7);
        grid_.add(greenValue_, 2, 7);
        grid_.add(blueSlider_, 1, 8);
        grid_.add(blueValue_, 2, 8);

        name_.setText("Player");
        name_.textProperty().addListener((observable, oldValue, newValue) -> {
            checkOkBtn.setDisable(name.getText().trim().isEmpty() || name_.getText().trim().isEmpty());
        });

        HBox hBox_ = new HBox();
        hBox_.getChildren().add(new Label("Tên nhân vât:"));
        hBox_.getChildren().add(name_);
        hBox_.setPadding(new Insets(0, 20, 0, 20));
        hBox_.setSpacing(10);
        // hBox.setAlignment(Pos.CENTER);
        hBox_.getChildren().add(canvas_);
        hBox_.getChildren().add(tabPane2);

        VBox vBox_ = new VBox();
        vBox_.getChildren().add(hBox_);

        tab_.setClosable(false);
        tab_.setContent(grid_);

        tab1_.setClosable(false);
        tab1_.setContent(gridPane_);
        tabPane2.getTabs().addAll(tab_, tab1_);

        vBox_.getChildren().add(tabPane2);

        hBox1.getChildren().add(vBox_);
        hBox1.setSpacing(10);
        // hBox1.setBorder(new Border(new BorderStroke()));
        dialog.getDialogPane().setContent(hBox1);

        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {

                Sprite pl, pl_;

                gc.setFill(Color.rgb(255, 0, 255));
                gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());

                gc_.setFill(Color.rgb(255, 0, 255));
                gc_.fillRect(0, 0, canvas_.getWidth(), canvas_.getHeight());

                if (tab.isSelected()) {
                    if (cus.isSelected()) {
                        redSlider.setDisable(false);
                        redValue.setDisable(false);
                        blueSlider.setDisable(false);
                        blueValue.setDisable(false);
                        greenSlider.setDisable(false);
                        greenValue.setDisable(false);
                    } else {

                        redSlider.setDisable(true);
                        redValue.setDisable(true);
                        blueSlider.setDisable(true);
                        blueValue.setDisable(true);
                        greenSlider.setDisable(true);
                        greenValue.setDisable(true);
                    }
                    int clo = 0;

                    switch (((RadioButton) colorGroup.getSelectedToggle()).getId()) {
                        case "r":
                            clo = ConstGame.InfoObject.SELECTED_PLAYER_COLOR_RED;
                            break;
                        case "b":
                            clo = ConstGame.InfoObject.SELECTED_PLAYER_COLOR_BLUE;
                            break;
                        case "y":
                            clo = ConstGame.InfoObject.SELECTED_PLAYER_COLOR_YELLOW;
                            break;
                        case "g":
                            clo = ConstGame.InfoObject.SELECTED_PLAYER_COLOR_GREEN;
                            break;
                        case "c":
                            clo = ConstGame.InfoObject.customColor(
                                    Integer.parseInt(redValue.getText()),
                                    Integer.parseInt(greenValue.getText()),
                                    Integer.parseInt(blueValue.getText()));
                            break;
                    }

                    pl = ((RadioButton) sexGroup.getSelectedToggle()).getId().equals("m") ? Sprite.player_down(clo)
                            : Sprite.player_nu_down(clo);
                    gc.drawImage(pl.getFxImage((int) canvas.getWidth()), 0, 0);
                }
                if (tab1.isSelected()) {
                    pl = Sprite
                            .player_down_skin(Integer.parseInt(((RadioButton) skinGroup.getSelectedToggle()).getId()));
                    gc.drawImage(pl.getFxImage((int) canvas.getWidth()), 0, 0);
                }

                if (tab_.isSelected()) {
                    if (cus_.isSelected()) {
                        redSlider_.setDisable(false);
                        redValue_.setDisable(false);
                        blueSlider_.setDisable(false);
                        blueValue_.setDisable(false);
                        greenSlider_.setDisable(false);
                        greenValue_.setDisable(false);
                    } else {

                        redSlider_.setDisable(true);
                        redValue_.setDisable(true);
                        blueSlider_.setDisable(true);
                        blueValue_.setDisable(true);
                        greenSlider_.setDisable(true);
                        greenValue_.setDisable(true);
                    }
                    int clo = 0;

                    switch (((RadioButton) colorGroup_.getSelectedToggle()).getId()) {
                        case "r":
                            clo = ConstGame.InfoObject.SELECTED_PLAYER_COLOR_RED;
                            break;
                        case "b":
                            clo = ConstGame.InfoObject.SELECTED_PLAYER_COLOR_BLUE;
                            break;
                        case "y":
                            clo = ConstGame.InfoObject.SELECTED_PLAYER_COLOR_YELLOW;
                            break;
                        case "g":
                            clo = ConstGame.InfoObject.SELECTED_PLAYER_COLOR_GREEN;
                            break;
                        case "c":
                            clo = ConstGame.InfoObject.customColor(
                                    Integer.parseInt(redValue_.getText()),
                                    Integer.parseInt(greenValue_.getText()),
                                    Integer.parseInt(blueValue_.getText()));
                            break;
                    }

                    pl_ = ((RadioButton) sexGroup_.getSelectedToggle()).getId().equals("m") ? Sprite.player_down(clo)
                            : Sprite.player_nu_down(clo);
                    gc_.drawImage(pl_.getFxImage((int) canvas_.getWidth()), 0, 0);
                }
                if (tab1_.isSelected()) {
                    pl_ = Sprite
                            .player_down_skin(Integer.parseInt(((RadioButton) skinGroup_.getSelectedToggle()).getId()));
                    gc_.drawImage(pl_.getFxImage((int) canvas_.getWidth()), 0, 0);
                }
            }
        };

        timer.start();

        skinGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                skin1_.setDisable(false); // 0
                skin2_.setDisable(false); // 2
                skin3_.setDisable(false);
                skin4_.setDisable(false);
                skin5_.setDisable(false);
                skin6_.setDisable(false);
                skin7_.setDisable(false);
                skin8_.setDisable(false); // 1

                switch (((RadioButton) newValue).getId()) {
                    case "0":
                        skin1_.setDisable(true);
                        break;
                    case "1":
                        skin8_.setDisable(true);
                        break;
                    case "2":
                        skin2_.setDisable(true);
                        break;
                    case "3":
                        skin3_.setDisable(true);
                        break;
                    case "4":
                        skin4_.setDisable(true);
                        break;
                    case "5":
                        skin5_.setDisable(true);
                        break;
                    case "6":
                        skin6_.setDisable(true);
                        break;
                    case "7":
                        skin7_.setDisable(true);
                        break;
                }
            }
        });

        skinGroup_.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                skin1.setDisable(false); // 0
                skin2.setDisable(false); // 2
                skin3.setDisable(false);
                skin4.setDisable(false);
                skin5.setDisable(false);
                skin6.setDisable(false);
                skin7.setDisable(false);
                skin8.setDisable(false); // 1

                switch (((RadioButton) newValue).getId()) {
                    case "0":
                        skin1.setDisable(true);
                        break;
                    case "1":
                        skin8.setDisable(true);
                        break;
                    case "2":
                        skin2.setDisable(true);
                        break;
                    case "3":
                        skin3.setDisable(true);
                        break;
                    case "4":
                        skin4.setDisable(true);
                        break;
                    case "5":
                        skin5.setDisable(true);
                        break;
                    case "6":
                        skin6.setDisable(true);
                        break;
                    case "7":
                        skin7.setDisable(true);
                        break;
                }
            }
        });

        dialog.setResultConverter(btn -> {
            timer.stop();
            String name1 = name.getText().trim();
            String name2 = name_.getText().trim();
            RadioButton sexBtn = (RadioButton) sexGroup.getSelectedToggle();
            RadioButton sexBtn_ = (RadioButton) sexGroup_.getSelectedToggle();
            Pair<String, Pair<Integer, Boolean>> pair1 = null, pair2 = null;

            if (btn == checkOk) {
                if (tab.isSelected()) {
                    RadioButton button = (RadioButton) colorGroup.getSelectedToggle();

                    // System.out.println("color: " + button.getId());
                    // System.out.println("Sex: " + sexBtn.getId());

                    switch (button.getId()) {
                        case "r":
                            pair1 = new Pair<>(name1, new Pair<>(ConstGame.InfoObject.SELECTED_PLAYER_COLOR_RED,
                                    sexBtn.getId().equals("m")));
                            break;
                        case "b":
                            pair1 = new Pair<>(name1, new Pair<>(ConstGame.InfoObject.SELECTED_PLAYER_COLOR_BLUE,
                                    sexBtn.getId().equals("m")));
                            break;
                        case "y":
                            pair1 = new Pair<>(name1, new Pair<>(ConstGame.InfoObject.SELECTED_PLAYER_COLOR_YELLOW,
                                    sexBtn.getId().equals("m")));
                            break;
                        case "g":
                            pair1 = new Pair<>(name1, new Pair<>(ConstGame.InfoObject.SELECTED_PLAYER_COLOR_GREEN,
                                    sexBtn.getId().equals("m")));
                            break;
                        case "c":
                            int clo = ConstGame.InfoObject.customColor(Integer.parseInt(redValue.getText()),
                                    Integer.parseInt(greenValue.getText()), Integer.parseInt(blueValue.getText()));
                            pair1 = new Pair<>(name1, new Pair<>(clo, sexBtn.getId().equals("m")));
                    }
                }
                if (tab1.isSelected()) {
                    int clo = Integer.parseInt(((RadioButton) skinGroup.getSelectedToggle()).getId());
                    pair1 = new Pair<>(name1, new Pair<>(clo, null));
                }

                if (tab_.isSelected()) {
                    RadioButton button = (RadioButton) colorGroup_.getSelectedToggle();

                    // System.out.println("color: " + button.getId());
                    // System.out.println("Sex: " + sexBtn.getId());

                    switch (button.getId()) {
                        case "r":
                            pair2 = new Pair<>(name2, new Pair<>(ConstGame.InfoObject.SELECTED_PLAYER_COLOR_RED,
                                    sexBtn.getId().equals("m")));
                            break;
                        case "b":
                            pair2 = new Pair<>(name2, new Pair<>(ConstGame.InfoObject.SELECTED_PLAYER_COLOR_BLUE,
                                    sexBtn.getId().equals("m")));
                            break;
                        case "y":
                            pair2 = new Pair<>(name2, new Pair<>(ConstGame.InfoObject.SELECTED_PLAYER_COLOR_YELLOW,
                                    sexBtn.getId().equals("m")));
                            break;
                        case "g":
                            pair2 = new Pair<>(name2, new Pair<>(ConstGame.InfoObject.SELECTED_PLAYER_COLOR_GREEN,
                                    sexBtn.getId().equals("m")));
                            break;
                        case "c":
                            int clo = ConstGame.InfoObject.customColor(Integer.parseInt(redValue_.getText()),
                                    Integer.parseInt(greenValue_.getText()), Integer.parseInt(blueValue_.getText()));
                            pair2 = new Pair<>(name2, new Pair<>(clo, sexBtn_.getId().equals("m")));
                    }
                }
                if (tab1_.isSelected()) {
                    int clo = Integer.parseInt(((RadioButton) skinGroup_.getSelectedToggle()).getId());
                    pair2 = new Pair<>(name2, new Pair<>(clo, null));
                }

                return new Pair<>(pair1, pair2);

            }
            return null;
        });

        Optional<Pair<Pair<String, Pair<Integer, Boolean>>, Pair<String, Pair<Integer, Boolean>>>> optional;
        optional = dialog.showAndWait();
        Pair<Pair<String, Pair<Integer, Boolean>>, Pair<String, Pair<Integer, Boolean>>> res = null;

        if(optional.isPresent()){
            res = optional.get();
        }

//        System.out.println(res.toString());
        return res;
    }

}