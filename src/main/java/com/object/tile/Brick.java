package com.object.tile;

import com.graphics.ScreenGame;
import com.graphics.Sprite;

public class Brick extends Tile {
    protected boolean destroyed;
    protected boolean beforeDestroy;
    protected int id = -1;
    protected int t = 0;
    protected int old;

    public Brick(float x, float y, Sprite sprite) {
        super(x, y, sprite);
        destroyed = false;
        beforeDestroy = false;
        old = 100;
    }
    public Brick(float x, float y, Sprite sprite,int id) {
        this(x,y,sprite);
        this.id = id;
    }

    @Override
    public void render(ScreenGame screenGame, int now) {
        if (!destroyed) {
            render(screenGame);
        } else {
            if(old != now){
                t++;
                old=now;
            }
            screenGame.drawSprite(Sprite.movingSprite(Sprite.brick_exploded,
                    Sprite.brick_exploded1, Sprite.brick_exploded2, t), getX(), getY());
        }
    }

    public final boolean isDestroyed() {
        return destroyed;
    }

    public void destroy() {
        destroyed = true;
    }

    public void setBeforeDestroy() {
        beforeDestroy = true;
    }

    public boolean isBeforeDestroy() {
        return beforeDestroy;
    }


    public int getId() {
        return id;
    }
}
