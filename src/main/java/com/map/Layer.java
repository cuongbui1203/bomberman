package com.map;

import com.graphics.ImageMerger;
import com.graphics.Render;
import com.graphics.ScreenGame;
import com.object.character.player.Bomber;
import javafx.scene.image.Image;


public class Layer implements Render{

    private Image brickImage;
    private MapLevel map;

    public Layer() {
    }


    public void load(Bomber player) {
        map.player = player;
        map.loadObj();
        brickImage = ImageMerger.createStaticImage3(map.bricks, map.y, map.x);
    }

    public void setMap(MapLevel map) {
        this.map = map;
    }

    public Image getBrickImage() {
        return brickImage;
    }

    public MapLevel getMap() {
        return map;
    }


    @Override
    public void render(ScreenGame screenGame, int now) {
        map.render(screenGame,now);
    }

    @Override
    public void update() {
        map.update();
        if(map.remove()) {
            brickImage = ImageMerger.createStaticImage3(map.bricks, map.y, map.x);
        }
    }
    @Override
    public void render(ScreenGame screenGame) {
        map.render(screenGame);
    }

    public void reset(){
        map.reset();
        brickImage = null;
    }

}
